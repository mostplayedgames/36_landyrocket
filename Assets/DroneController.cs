﻿using UnityEngine;
using System.Collections;

public class DroneController : MonoBehaviour 
{
	public static DroneController instance;

	public static float MIN_DRONE_SPEED = 2.0f;
	public static float MAX_DRONE_SPEED = 4.0f;
	public enum BallDirection
	{
		LEFT,
		RIGHT,
		STOP
	}

	public BallDirection m_curDirection;

	public float m_ballRotSpeed = 5.0f;
	public float m_droneXMoveSpeed = 5.0f;
	public float m_droneYMoveSpeed = -5.0f;
	public float m_droneMoveProgression = 0;

	public GameObject m_ball;
	public GameObject m_shelf;
	public AudioClip m_hitSfx;


	// Use this for initialization
	void Start () 
	{
		instance = this;
		m_curDirection = BallDirection.LEFT;
	}
	
	// Update is called once per frame
	void Update () 
	{
		DroneTransform ();
	}

	public void Die()
	{
		m_droneMoveProgression = 0;
	}

	public void BottleLanded()
	{
		m_curDirection = BallDirection.STOP;
		this.GetComponent<Rigidbody2D> ().isKinematic = true;
		this.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		m_droneMoveProgression += 0.5f;
	}

	public void DroneTransform()
	{

		if (this.transform.position.y > 31.0f) {
			m_droneYMoveSpeed = GenRandVal () * -1;
		} else if (this.transform.position.y < -10) {
			m_droneYMoveSpeed = GenRandVal ();
		}

		switch (m_curDirection) {
		case BallDirection.LEFT:
			{
				this.transform.position += new Vector3 (-m_droneXMoveSpeed * Time.deltaTime, m_droneYMoveSpeed * Time.deltaTime, 0);
			}
			break;

		case BallDirection.RIGHT:
			{
				this.transform.position += new Vector3 (m_droneXMoveSpeed * Time.deltaTime, m_droneYMoveSpeed * Time.deltaTime, 0);
			}
			break;

		case BallDirection.STOP:
			{
				break;
			}
		}
	}

	public void ChageDirection()
	{
		this.GetComponent<Rigidbody2D> ().isKinematic = false;
		switch (m_curDirection) 
		{
		case BallDirection.LEFT:
			{
				m_curDirection = BallDirection.RIGHT;
			}
			break;

		case BallDirection.RIGHT:
			{
				m_curDirection = BallDirection.LEFT;
			}
			break;

		case BallDirection.STOP:
			{
				m_curDirection = BallDirection.RIGHT;
				break;
			}
		}
	}

	public void OnCollisionEnter2D(Collision2D p_other)
	{
		if (p_other.gameObject.name == "WaterBottle") 
		{
			BottleLanded ();
			return;
		}

		ZAudioMgr.Instance.PlaySFX (m_hitSfx);
		m_droneXMoveSpeed = GenRandVal ();
		ChageDirection ();
		ChangeHorizontalSpeed ();
	}

	private void ChangeHorizontalSpeed()
	{
		float randVal = Random.Range (0, 100);
		m_droneYMoveSpeed *= (randVal >= 50) ? -1 : 1;
	}

	private float GenRandVal()
	{
		return Random.Range(MIN_DRONE_SPEED + m_droneMoveProgression, MAX_DRONE_SPEED + m_droneMoveProgression);
	}
}
