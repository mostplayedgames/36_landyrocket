﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallex : MonoBehaviour {

	public Vector3 parallaxSpeed;
	Vector3 initialPosition;
	bool isMoving = false;
	// Use this for initalization
	void Awake () {
		initialPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoving)
			this.transform.localPosition -= parallaxSpeed * Time.deltaTime;
	}

	public void StartParallax()
	{
		isMoving = true;
	}

	public void StopParallax()
	{
		isMoving = false;
	}

	public void Reset()
	{
		this.transform.position = initialPosition;
		isMoving = false;
	}
}
