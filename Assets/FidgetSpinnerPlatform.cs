﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FidgetSpinnerPlatform : MonoBehaviour {

	public float m_rotationSpeed;

	public GameObject m_Platform1;
	public GameObject m_Platform2;
	public GameObject m_Platform3;

	private bool m_isMoveVertical = false;
	private string m_currentPlatform = "";


	private static FidgetSpinnerPlatform s_instance;
	public static FidgetSpinnerPlatform instance()
	{
		return s_instance;
	}

	// Use this for initialization
	void Start () 
	{
		m_rotationSpeed = 15;
		s_instance = this;
		BottleLanded ("Shelf1", true);
	}
	
	// Update is called once per frame
	void Update () 
	{
		rotateObj (m_Platform1);
		rotateObj (m_Platform2);
		rotateObj (m_Platform3);

		this.transform.localEulerAngles += new Vector3 (0, 0, m_rotationSpeed * Time.deltaTime);
	}

	public void UpdateRotSpeed(float p_level)
	{
		if (p_level < 9)
			return;
		
		m_rotationSpeed += 10;
	}

	public string GetCurrentPlatformName()
	{
		return m_currentPlatform;
	}

	public void BottleLanded(string p_currentPlatformName, bool p_showArrow = false)
	{
		m_currentPlatform = p_currentPlatformName;

		if (p_currentPlatformName == "Shelf1") 
		{
			int randomVal = Random.Range (0, 100);

			if (randomVal > 49) {
				m_Platform2.gameObject.SetActive (true);
				m_Platform2.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform3.gameObject.SetActive (false);
			} else {
				m_Platform3.gameObject.SetActive (true);
				m_Platform3.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform2.gameObject.SetActive (false);
			}

			m_Platform1.gameObject.SetActive (true);
			m_Platform1.transform.GetChild (0).transform.gameObject.SetActive (false);
		} 
		else if (p_currentPlatformName == "Shelf2") 
		{
			int randomVal = Random.Range (0, 100);

			if (randomVal > 49) {
				m_Platform1.gameObject.SetActive (true);
				m_Platform1.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform3.gameObject.SetActive (false);
			} else {
				m_Platform3.gameObject.SetActive (true);
				m_Platform3.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform1.gameObject.SetActive (false);
			}

			m_Platform2.gameObject.SetActive (true);
			m_Platform2.transform.GetChild (0).transform.gameObject.SetActive (false);

		} 
		else if (p_currentPlatformName == "Shelf3") 
		{
			int randomVal = Random.Range (0, 100);

			if (randomVal > 49) {
				m_Platform1.gameObject.SetActive (true);
				m_Platform1.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform2.gameObject.SetActive (false);
			} else {
				m_Platform2.gameObject.SetActive (true);
				m_Platform2.transform.GetChild (0).transform.gameObject.SetActive (true);

				m_Platform1.gameObject.SetActive (false);
			}

			m_Platform3.gameObject.SetActive (true);
			m_Platform3.transform.GetChild (0).transform.gameObject.SetActive (false);

		}
	}

	public void MoveVertical(bool p_isMoveVertical)
	{
		if (!p_isMoveVertical) 
		{
			m_rotationSpeed = 15;
			LeanTween.cancel (this.gameObject);		
			BottleLanded ("Shelf1", true);

			return;
		}

		if (m_isMoveVertical) 
		{
			return;
		}

		LeanTween.moveLocalY (this.gameObject, this.transform.localPosition.y + 10, 2.0f).setLoopPingPong ();

		m_isMoveVertical = p_isMoveVertical;

	}

	// + EDIT JOSH 20170714
	// this function makes sure that the platforms faces upward
	public void rotateObj(GameObject p_obj)
	{
		Vector3 hitPoint = p_obj.transform.localPosition + new Vector3(0, 0, 1);

		Vector3 targetDir = hitPoint - p_obj.transform.localPosition;

		float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
		p_obj.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
	}
}
