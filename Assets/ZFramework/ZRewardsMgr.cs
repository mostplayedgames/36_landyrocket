﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

using UnityEngine.Advertisements;

public enum REWARD_TYPES
{
	SHOP,
	SHARE,
	RATEUS,
	MOREGAMES,
	WATCHVIDEO,
	LIKEUS,
	FOLLOWUS,
	FOLLOW_TWITCH,
	SUBSCRIBE,
	LEADERBOARD,
	CHANGEMODE,
	DAILYREWARD,
	FREECOINS,
	IAPCOIN1,
}

[System.Serializable]
public class ZRewardData{
	public REWARD_TYPES types;
	public string displayName;
	public Sprite icon;
	public Color baseColor;
	public Color highlightColor;
	public int rewardValue;
	public int timeInitial;
	public int timeReccuring;
}

public class ZRewardsMgr : MonoBehaviour {

	public static ZRewardsMgr instance;

	public ZRewardData m_rewardNewChar;
	public ZRewardData m_rewardWatchVideo;

	public ZRewardData m_rewardShare;
	public ZRewardData m_rewardLikeUs;
	public ZRewardData m_rewardFollowUs;
	public ZRewardData m_rewardSubscribe;
	public ZRewardData m_rewardFollowTwitch;
	public ZRewardData m_rewardMoreGames;
	public ZRewardData m_rewardRateUs;
	public ZRewardData m_rewardLeaderboard;
	public ZRewardData m_rewardShop;
	public ZRewardData m_rewardChangeMode;

	public ZRewardData m_rewardDaily;
	public ZRewardData m_rewardIAP;

	public GameObject m_objectGetCash;
	public GameObject m_objectShop;
	public GameObject m_objectCasino;


	public GameObject m_objectButtons;
	public Image m_imageButton;
	public Image m_imageIcon;
	public Text m_text;
	public Text m_textDisplay;
	public Text m_textEarned;

	public AudioClip m_audioButton;

	public string REWARD_TITLE;
	public string REWARD_MESSAGE;

	//public int m_rewardValue = 100;

	REWARD_TYPES m_currentRewardType;
	ZRewardData m_currentRewardData;

	float m_currentIncentifiedAdsTime = 30;
	float m_currentRateUsTime = 30;
	float m_currentLikeUsTime = 30;
	float m_currentMoreGamesTime = 30;
	float m_currentFollowUsTime = 30;
	float m_currentSubscribeTime	= 30;
	float m_currentFollowTwitchTime	= 30;
	float m_currentShareTime = 30;
	float m_currentLeaderboardTime = 30;
	float m_currentShopTime = 30;
	float m_currentChangeModeTime = 30;
	float m_currentFreeCoinsTime = 30;
	float m_currentIAPTime = 30;


	DateTime m_currentDate;
	DateTime m_loadedDate;

	Color m_colorBase;
	Color m_colorHighlight;
	bool m_startInterrupt;
	int m_currentRewardValue;

	// Use this for initialization
	void Start () {

		instance = this;

		if (!PlayerPrefs.HasKey ("rewardsShare")) {
			PlayerPrefs.SetInt ("rewardsShare", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardLikeUs")) {
			PlayerPrefs.SetInt ("rewardLikeUs", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardFollowUs")) {
			PlayerPrefs.SetInt ("rewardFollowUs", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardMoreGames")) {
			PlayerPrefs.SetInt ("rewardMoreGames", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardRateUs")) {
			PlayerPrefs.SetInt ("rewardRateUs", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardSubscribe")) {
			PlayerPrefs.SetInt("rewardSubscribe", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardFollowTwitch")) {
			PlayerPrefs.SetInt ("rewardFollowTwitch", 0);
		}

		if (!PlayerPrefs.HasKey ("updateVersion3")) {
			PlayerPrefs.SetInt ("updateVersion3", 1);
			PlayerPrefs.SetInt ("rewardRateUs", 0);
			PlayerPrefs.SetInt ("rewardMoreGames", 0);
		}

		// + EDIT JOSH 
		// Set initial daily reward time
		if (!PlayerPrefs.HasKey ("rewardDaily")) 
		{
			DateTime curDateTime = DateTime.Now;
			PlayerPrefs.SetString ("rewardDaily", "" + curDateTime);
		}

		DateTime loadTime;
		if (DateTime.TryParse (PlayerPrefs.GetString ("rewardDaily"), out loadTime)) 
		{
			m_loadedDate = loadTime;
			Debug.Log ("LoadedTime: " + m_loadedDate);
		}
			
		m_currentIncentifiedAdsTime = m_rewardWatchVideo.timeInitial;//30;//180;
		m_currentRateUsTime = m_rewardRateUs.timeInitial;
		m_currentLikeUsTime = m_rewardLikeUs.timeInitial;
		m_currentSubscribeTime = m_rewardSubscribe.timeInitial;
		m_currentMoreGamesTime = m_rewardMoreGames.timeInitial;
		m_currentFollowUsTime = m_rewardFollowUs.timeInitial;
		m_currentShareTime = m_rewardShare.timeInitial;
		m_currentLeaderboardTime = m_rewardLeaderboard.timeInitial;
		m_currentShopTime = m_rewardShop.timeInitial;
		m_currentChangeModeTime = m_rewardChangeMode.timeInitial;
		m_currentFollowTwitchTime = m_rewardFollowTwitch.timeInitial;
		m_currentFreeCoinsTime = m_rewardDaily.timeInitial;
		m_currentIAPTime = m_rewardIAP.timeInitial;

		#if UNITY_IPHONE
		m_currentIncentifiedAdsTime *= 0.7f;
		m_currentRateUsTime *= 0.7f;
		m_currentLikeUsTime *= 0.7f;
		m_currentSubscribeTime *= 0.7f;
		m_currentMoreGamesTime *= 0.7f;
		m_currentFollowUsTime *= 0.7f;
		m_currentShareTime *= 0.7f;
		m_currentLeaderboardTime *= 0.7f;
		m_currentShopTime *= 0.7f;
		m_currentChangeModeTime *= 0.7f;
		m_currentFollowTwitchTime *= 0.7f;
		m_currentFreeCoinsTime *= 0.7f;
		m_currentIAPTime *= 0.7f;
		#endif

		m_startInterrupt = false;
	}
	
	// Update is called once per frame
	void Update () {
		m_currentIncentifiedAdsTime -= Time.deltaTime;
		m_currentRateUsTime -= Time.deltaTime;
		m_currentLikeUsTime -= Time.deltaTime;
		m_currentSubscribeTime -= Time.deltaTime;
		m_currentFollowTwitchTime -= Time.deltaTime;
		m_currentMoreGamesTime -= Time.deltaTime;
		m_currentFollowUsTime -= Time.deltaTime;
		m_currentShareTime -= Time.deltaTime;
		m_currentLeaderboardTime -= Time.deltaTime;
		m_currentShopTime -= Time.deltaTime;
		m_currentChangeModeTime -= Time.deltaTime;
		m_currentFreeCoinsTime -= Time.deltaTime;
		m_currentIAPTime -= Time.deltaTime;
	}

	public bool WillShowShopButton()
	{
		if (m_currentShopTime <= 0)
			return true;
		return false;
	}

	public bool WillShowReward()
	{
		if (m_currentIncentifiedAdsTime <= 0)
			return true;
		else if (m_currentFreeCoinsTime <= 0)
			return true;
		else if (m_currentLeaderboardTime <= 0)
			return true;
		else if (m_currentChangeModeTime <= 0)
			return true;
		return false;
	}

	public bool WillShowRewardSuccess()
	{
		m_currentDate = DateTime.Now;

		/*if (m_currentDate.Day >= m_loadedDate.Day &&
			m_currentDate.Hour >= m_loadedDate.Hour)
		{
			return true;
		}
		else */
		if (m_currentRateUsTime <= 0 && PlayerPrefs.GetInt ("rewardRateUs") < ZGameMgr.instance.UPDATE_NUMBER)
			return true;
		else if (m_currentLikeUsTime <= 0 && PlayerPrefs.GetInt ("rewardLikeUs") <= 0)
			return true;
		else if (m_currentMoreGamesTime <= 0 && PlayerPrefs.GetInt ("rewardMoreGames") <= 0)
			return true;
		else if (m_currentFollowUsTime <= 0 && PlayerPrefs.GetInt ("rewardFollowUs") <= 0)
			return true;
		else if (m_currentSubscribeTime <= 0 && PlayerPrefs.GetInt ("rewardSubscribe") <= 0)
			return true;
		else if (m_currentFollowTwitchTime <= 0 && PlayerPrefs.GetInt ("rewardFollowTwitch") <= 0)
			return true;
		else if (m_currentIncentifiedAdsTime <= 0)
			return true;
		else if (m_currentFreeCoinsTime <= 0)
			return true;
		else if (m_currentIAPTime <= 0)
			return true;
		//else if (false && m_currentShareTime <= 0)
		//	return true;
		//else if (false && m_currentLeaderboardTime <= 0)
		//	return true;
		return false;
	}

	public int GetTimeDifference()
	{
		m_currentDate = DateTime.Now;
		DateTime dateTarget = m_loadedDate.AddDays (1);

		TimeSpan newTime = (dateTarget - m_currentDate);

		return newTime.Minutes;
	}

	public void ShowShopButton(bool force = false){
		if (m_currentShopTime <= 0 || force) {
			m_currentShopTime = m_rewardShop.timeReccuring;//30;//180;
			//ShowButton (m_rewardShop);

			LeanTween.delayedCall (2.5f, ShowShopButtonRoutine);

			//AnimateButton ();

			m_currentRewardType = REWARD_TYPES.SHOP;
		}
	}

	void ShowShopButtonRoutine()
	{
		if (GameScene.instance.m_eState == GAME_STATE.SHOP)
			return;
		
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
//		GameScene.instance.ShowShop ();
		GameScene.instance.UnlockNewItem();

		//		m_objectShop.SetActive (true);
//		//m_objectCasino.SetActive(true);
//		//m_objectCasino.GetComponent<CasinoShop> ().SetupScene ();
//		m_objectShop.GetComponent<ShopScene> ().SetupScene (false);
//		GameScene.instance.m_eState = GAME_STATE.SHOP;
	}

	public void ShowReward(){

		// SHOW REWARDED AD
		if (m_currentIncentifiedAdsTime <= 0) {
			m_currentIncentifiedAdsTime = m_rewardWatchVideo.timeReccuring;//30;//180;
			ShowButton (m_rewardWatchVideo);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.WATCHVIDEO;
		} 
		// LEADERBOARD
		else if (m_currentLeaderboardTime <= 0 ) {
			m_currentLeaderboardTime = m_rewardLeaderboard.timeReccuring;

			ShowButton (m_rewardLeaderboard);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LEADERBOARD;
		} 
		// CHANGEMODE
		else if (m_currentChangeModeTime <= 0 ) {
			m_currentChangeModeTime = m_rewardChangeMode.timeReccuring;

			ShowButton (m_rewardChangeMode);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.CHANGEMODE;
		} 

	}

	public void ShowRewardSuccess(){

		if (m_currentRateUsTime <= 0 && PlayerPrefs.GetInt ("rewardRateUs") < ZGameMgr.instance.UPDATE_NUMBER) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring + 120;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring;

			ShowButton (m_rewardRateUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.RATEUS;
		}

		// LIKE US
		else if (m_currentLikeUsTime <= 0 && PlayerPrefs.GetInt ("rewardLikeUs") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring + 120;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring;

			ShowButton (m_rewardLikeUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LIKEUS;
		} 

		// SUBSCRIBE
		else if (m_currentSubscribeTime <= 0 && PlayerPrefs.GetInt ("rewardSubscribe") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring + 120;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring;

			ShowButton (m_rewardSubscribe);
			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.SUBSCRIBE;
		}

		//FOLLOW TWITCH

		else if (m_currentFollowTwitchTime <= 0 && PlayerPrefs.GetInt ("rewardFollowTwitch") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring + 120;

			ShowButton (m_rewardFollowTwitch);
			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FOLLOW_TWITCH;
		}

		// MORE GAMES
		else if (m_currentMoreGamesTime <= 0 && PlayerPrefs.GetInt ("rewardMoreGames") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring + 120;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			ShowButton (m_rewardMoreGames);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.MOREGAMES;
		}

		// FOLLOW US
		else if (m_currentFollowUsTime <= 0 && PlayerPrefs.GetInt ("rewardFollowUs") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring + 120;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			ShowButton (m_rewardFollowUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FOLLOWUS;
		}

//		// DAILY_REWARD
//		else if (m_currentDate.Day >= m_loadedDate.Day &&
//		        m_currentDate.Hour >= m_loadedDate.Hour) {
//
//		}
//
		/*else if (m_currentDate.Day >= m_loadedDate.Day + 1 &&
		        m_currentDate.Hour >= m_loadedDate.Hour + 1)
		{

			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			ShowButton (m_rewardDaily);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.DAILYREWARD;

			// Set New Time
			PlayerPrefs.SetString ("rewardDaily", "" + m_currentDate);
			m_loadedDate = m_currentDate;

		}*/

		else if (m_currentFreeCoinsTime <= 0 ) {
			m_currentFreeCoinsTime = m_rewardDaily.timeReccuring;//30;//180;
			ShowButton (m_rewardDaily);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FREECOINS;
		} 

		// SHOW REWARDED AD
		else if (m_currentIncentifiedAdsTime <= 0 ) {
			m_currentIncentifiedAdsTime = m_rewardWatchVideo.timeReccuring;//30;//180;
			ShowButton (m_rewardWatchVideo);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.WATCHVIDEO;
		} 

		// SHOW REWARDED AD
		else if (m_currentIAPTime <= 0 ) {
			m_currentIAPTime = m_rewardIAP.timeReccuring;//30;//180;
			ShowButton (m_rewardIAP);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.IAPCOIN1;
		} 

		// SHARE
		/*else if (false && m_currentShareTime <= 0 ){// && PlayerPrefs.GetInt ("rewardShare") <= 0 ) {
			m_currentShareTime = m_rewardShare.timeReccuring;

			ShowButton (m_rewardShare);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.SHARE;
		} 

		// LEADERBOARD
		else if (false && m_currentLeaderboardTime <= 0 ) {
			m_currentLeaderboardTime = m_rewardLeaderboard.timeReccuring;

			ShowButton (m_rewardLeaderboard);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LEADERBOARD;
		} */
	}

	void AnimateButton()
	{
		LeanTween.cancel (m_objectButtons);
		m_objectButtons.transform.localScale = new Vector3 (0.65f, 0.65f, 0.65f);
		LeanTween.scale (m_objectButtons, m_objectButtons.transform.localScale + new Vector3 (0.1f, 0.1f, 0.1f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}

	void ShowButton(ZRewardData data)
	{
		m_objectButtons.SetActive (true);

		m_imageIcon.overrideSprite = data.icon;
		m_imageButton.color = data.baseColor;
		if (data.rewardValue > 0) {
			m_text.text = "EARN +" + data.rewardValue;
			m_textDisplay.text = data.displayName;
		} else {
			m_text.text = data.displayName;
			m_textDisplay.text = "ITS AWESOME";
		}


		//data.rewardValue = Mathf.FloorToInt (GameScene.instance.GetCurrentShopPrice () / 10f);
		
		m_colorBase = data.baseColor;
		m_colorHighlight = data.highlightColor;

		m_currentRewardData = data;

		int randomRewardValue;
		if (data.types == REWARD_TYPES.WATCHVIDEO) {
			 randomRewardValue = UnityEngine.Random.Range (40, 100);
			if (randomRewardValue > 90)
				m_currentRewardValue = 100;
			else if (randomRewardValue > 80)
				m_currentRewardValue = 90;
			else if (randomRewardValue > 70)
				m_currentRewardValue = 80;
			else if (randomRewardValue > 60)
				m_currentRewardValue = 70;
			else if (randomRewardValue > 50)
				m_currentRewardValue = 60;
			else
				m_currentRewardValue = 50;
			
			//m_currentRewardValue = UnityEngine.Random.Range (50, 100);//50;//GameScene.instance.m_currentTotalCoinAdd * 5;//Mathf.FloorToInt (GameScene.instance.GetCurrentShopPrice () / 10f);
			m_text.text = "EARN +" + m_currentRewardValue;
		}
		else if (data.types == REWARD_TYPES.FREECOINS) {
			 randomRewardValue = UnityEngine.Random.Range (5, 25);
			if (randomRewardValue > 20)
				m_currentRewardValue = 25;
			else if (randomRewardValue > 15)
				m_currentRewardValue = 20;
			else if (randomRewardValue > 10)
				m_currentRewardValue = 15;
			else
				m_currentRewardValue = 10;
			//m_currentRewardValue = UnityEngine.Random.Range (10, 25);//20;//GameScene.instance.m_currentTotalCoinAdd * 2;//Mathf.FloorToInt (GameScene.instance.GetCurrentShopPrice () / 10f);
			m_text.text = "EARN +" + m_currentRewardValue;
		}
		else
			m_currentRewardValue = data.rewardValue;

		ShowButtonAnimate ();
	}

	void ShowButtonAnimate()
	{
		if (!m_objectButtons.activeSelf)
			return;

		m_imageButton.color = m_colorBase;
		LeanTween.delayedCall (0.5f, ShowButtonAnimateDown);
	}

	void ShowButtonAnimateDown()
	{
		if (!m_objectButtons.activeSelf)
			return;

		m_imageButton.color = m_colorHighlight;
		LeanTween.delayedCall (0.5f, ShowButtonAnimate);
	}

	public void ActionButton(){
		m_objectButtons.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		switch (m_currentRewardType) {
		case REWARD_TYPES.FOLLOWUS:
			ZFollowUsMgr.Instance.FollowUs ();
			//PlayerPrefs.SetInt ("rewardFollowUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.LIKEUS:
			ZFollowUsMgr.Instance.LikeUs ();
			//PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SUBSCRIBE:
			ZFollowUsMgr.Instance.Subscribe ();
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.FOLLOW_TWITCH:
			ZFollowUsMgr.Instance.FollowTwitch ();
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.MOREGAMES:
			ZFollowUsMgr.Instance.MoreGames ();
			//PlayerPrefs.SetInt ("rewardMoreGames", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.RATEUS:
			ZRateUsMgr.Instance.RateUs ();
			//PlayerPrefs.SetInt ("rewardRateUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SHARE:
			//RewardPlayer ();
			ZFollowUsMgr.Instance.Share ();
			ZAnalytics.Instance.SendShareComplete ();
			//m_startInterrupt = true;
			//ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SHOP:
			m_objectShop.SetActive (true);
			ShopScene.instance.SetupScene ();
			ZAnalytics.Instance.SendShopComplete ();
			break;
		case REWARD_TYPES.FREECOINS:
			//ZRateUsMgr.Instance.RateUs();
			//PlayerPrefs.SetInt ("rewardRateUs", ZGameMgr.instance.UPDATE_NUMBER);
			RewardPlayerRoutine ();
//			m_startInterrupt = true;
//			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.WATCHVIDEO:
			ShowRewardedVideo ();
			break;
		case REWARD_TYPES.CHANGEMODE:
			GameScene.instance.ChangeMode ();
			break;
		case REWARD_TYPES.LEADERBOARD:
			if (ZGameMgr.instance.isOnline) {
				GameScene.instance.OpenLeaderboard ();
			} else {
				// No Connection
				GameScene.instance.m_objectNoInternet.SetActive (true);
			}
			break;
		case REWARD_TYPES.IAPCOIN1:
			if (ZGameMgr.instance.isOnline) {
				ZIAPMgr.Instance.PurchaseCoins ();
			} else {
				// No Connection
				GameScene.instance.m_objectNoInternet.SetActive (true);
			}
			break;
		}



	}

	void OnApplicationFocus( bool focusStatus )
	{
		if( !m_startInterrupt )
			return;
			
		if( m_currentRewardValue > 0 )
			RewardPlayer ();
		m_startInterrupt = false;

		switch (m_currentRewardType) {
		case REWARD_TYPES.FOLLOWUS:
			PlayerPrefs.SetInt ("rewardFollowUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendFollowUsComplete ();
			break;
		case REWARD_TYPES.LIKEUS:
			PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendLikeUsComplete ();
			break;
		case REWARD_TYPES.SUBSCRIBE:
			PlayerPrefs.SetInt ("rewardSubscribe", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendSubscribeComplete ();
			break;
		case REWARD_TYPES.FOLLOW_TWITCH:
			PlayerPrefs.SetInt ("rewardFollowTwitch", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendFollowTwitchComplete ();
			break;
		case REWARD_TYPES.MOREGAMES:
			PlayerPrefs.SetInt ("rewardMoreGames", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendMoreGamesComplete ();
			break;
		case REWARD_TYPES.RATEUS:
			PlayerPrefs.SetInt ("rewardRateUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendRateUsComplete ();
			break;
		case REWARD_TYPES.DAILYREWARD:
			break;
		}
	}
		
	public void NextDailyReward()
	{

	}

	/*void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	}*/
		
	void RewardPlayer()
	{
		if (ZGameMgr.instance.isOnline) {
			LeanTween.delayedCall (0.5f, RewardPlayerRoutine);
		} else {
			// No Connection
			GameScene.instance.m_objectNoInternet.SetActive(true);
		}
	}

	void RewardPlayerRoutine()
	{
		ZAnalytics.Instance.SendEarnCoinsEvent ("reward" + m_currentRewardType, m_currentRewardValue);
		GameScene.instance.AddCoins (m_currentRewardValue);
		m_objectGetCash.SetActive (true);
		m_textEarned.text = "+" + m_currentRewardValue;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		LeanTween.scale (m_objectGetCash, m_objectGetCash.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}

	void ShowShop()
	{
		m_objectShop.SetActive (true);
	}


	public void ShowRewardedVideo()
	{
		if (Advertisement.IsReady ("rewardedVideo")) {
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		}
		else {
			// No Connection
			GameScene.instance.m_objectNoInternet.SetActive(true);

		}
		//m_objectWatchVideo.SetActive (false);

		//ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			//LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			RewardPlayer ();
			ZAnalytics.Instance.SendEarnComplete ();
			//if (rewardedSource == 0)
			//	ContinueLevel ();
			//if (rewardedSource == 1) {
				//AddCoins (100);
				//m_objectEarned.SetActive (true);
				//new MobileNativeMessage("You get 100 Eagles", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			//}
			//m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			//LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			//LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}
}
