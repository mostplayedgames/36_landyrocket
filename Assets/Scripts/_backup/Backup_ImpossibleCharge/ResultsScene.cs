﻿using UnityEngine;
using System.Collections;

public class ResultsScene : ZGameScene {

	public static ResultsScene instance;

	void Awake()
	{
		instance = this;
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
			ZGameMgr.instance.ShowScene ("HomeScene");
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
	}
}
