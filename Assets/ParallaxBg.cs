﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjBounds
{
	public ObjBounds(float p_halfMinX, float p_halfMaxX, float p_fullMinX,float p_fullMaxX,float p_extends)
	{
		halfMinX = p_halfMinX;
		halfMaxX = p_halfMaxX;
		fullMinX = p_fullMinX;
		fullMaxX = p_fullMaxX;
		extends = p_extends;
	}

	public float halfMinX;
	public float halfMaxX;
	public float fullMinX;
	public float fullMaxX;
	public float extends;
};

public class ParallaxBg : MonoBehaviour 
{
	public static ParallaxBg instance;

	public List<GameObject> m_cityModels;
	public GameObject m_currentCity;
	public GameObject m_nextCity;
	public GameObject m_prevCity;

	void Awake () 
	{
		instance = this;

		//UpdateBackground (0);
	}

	public void InitBackground(float p_bottlePosX)
	{
		if (m_currentCity != null)
			return;
		
		m_currentCity = GetUnusedObj ();
		m_currentCity.gameObject.SetActive (true);
		m_currentCity.transform.position = new Vector3 (p_bottlePosX, m_currentCity.transform.position.y, m_currentCity.transform.position.z);
	}

	public void UpdateBackground(float p_bottlePosX)
	{
		ObjBounds cityBounds = GetObjectBounds (m_currentCity);

		if (p_bottlePosX > (cityBounds.extends + cityBounds.fullMaxX)) 
		{
			m_prevCity = m_currentCity;
			m_prevCity.gameObject.SetActive (false);
			m_currentCity = m_nextCity;
			m_nextCity = null;
		}
		// Current bottle position is greater than half of the city
		else if (p_bottlePosX > cityBounds.halfMinX) 
		{
			if (m_nextCity == null) 
			{
				m_nextCity = GetUnusedObj ();
				m_nextCity.gameObject.SetActive (true);
				float xPos = cityBounds.fullMaxX + cityBounds.extends;

				m_nextCity.transform.localPosition = new Vector3 (xPos,
					m_nextCity.transform.position.y,
					m_nextCity.transform.position.z);
			}
		}
	}

	private void RestObj()
	{
		foreach (GameObject obj in m_cityModels) 
		{
			obj.gameObject.SetActive (true);
		}
	}

	private ObjBounds GetObjectBounds(GameObject p_obj)
	{
		float extendsX = p_obj.GetComponent<BoxCollider2D> ().bounds.extents.x;

		float minHalf = p_obj.transform.localPosition.x - 
						(p_obj.GetComponent<BoxCollider2D> ().bounds.extents.x / 2);

		float maxHalf = p_obj.transform.localPosition.x + 
						(p_obj.GetComponent<BoxCollider2D> ().bounds.extents.x /2 );

		float minFull = p_obj.transform.localPosition.x - 
						p_obj.GetComponent<BoxCollider2D> ().bounds.extents.x;

		float maxFull = p_obj.transform.localPosition.x +
		                p_obj.GetComponent<BoxCollider2D> ().bounds.extents.x;

		return new ObjBounds (minHalf, maxHalf, minFull, maxFull, extendsX);
	}

	private GameObject GetUnusedObj()
	{
//		float randval = Random.Range (0, 100);
//		float remapped = remapValue (randval, 0, 100, 0, (m_cityModels.Count - 1));
//		float roundedOf = Mathf.RoundToInt (remapped);
////		if (m_cityModels [remapped].activeSelf) 
////		{
////			return m_cityModels [remapped];
////		}

		foreach (GameObject obj in m_cityModels) 
		{
			if (!obj.activeSelf) 
			{
				return obj;
			}
		}

//		for (int idx = 0; idx < m_cityModels.Count; idx++) 
//		{
//			if (roundedOf == idx) 
//			{
//				if (!m_cityModels [idx].activeSelf) {
//					return m_cityModels [idx];
//				} else {
//					GetUnusedObj ();
//				}
//			}
//		}
		return null;
	}


	private float remapValue(float p_value, float p_from1, float p_to1, float p_from2, float p_to2)
	{
		return (p_value - p_from1) / (p_to1 - p_from1) * (p_to2 - p_from2) + p_from2;
	}

	void Update () 
	{
		
	}
}
