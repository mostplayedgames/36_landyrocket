﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SBBall : MonoBehaviour {

	public List<AudioClip> m_listAudioBall;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) {
		ZAudioMgr.Instance.PlaySFXVolume (m_listAudioBall[Random.Range(0,100) % m_listAudioBall.Count], Random.Range(0.4f, 0.8f));
	}
}
