﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	START,
	IDLE,
	SHOP,
	AIMING,
	SHOOT,
	RESULTS
}

public enum GAME_MODES
{
	NORMAL_MODE = 0,
	ENDLESS_MODE,
	IMPOSSIBLE_MODE,
	ZEN_MODE,
	SPACE_MODE,
	BOTTLECAP_MODE,
	FREE_MODE,
	CAPLEVEL_MODE,
	TOWER_MODE,
	CHALLENGE_MODE,
	CITY_MODE,
	MARKER_MODE
}

public enum CityPrefabs
{
	BIRD = 16,
	CAR,
	CAT,
	CLOWN,
	HOTDOG,
	LAMPOST,
	TABLE,
	PHONE_BOOTH,
	SCAFFOLDING,
	TRUCK,
	WOMAN,
	STOP_LIGHT,
	DOG,
	BUS,
	BENCH,
	TREE,
	HYDRANT,
	BALLOON
}

public enum WindDirection
{
	LEFT,
	RIGHT
}
	
public enum ShopBirdFlapAnimType
{
	FLIP,
	FLAPPY,
	REVERSEFLIP,
	FLIPPY,
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectCharacter;
	public Sprite imagePortrait;
	public string m_textCharacterName;
	public Color colorTrailStart;
	public Color colorTrailEnd;
	public Color colorFeathers;
	public bool isUpdateWeatherColors;
	public Color colorScore;
	public Color colorCameraWeather;
	public Color colorSky;
	public bool willFlat;
	public float spinRate;
	public ShopBirdFlapAnimType flipAnimType;
	public int rarity;
}

[System.Serializable]
public class CityModeLevel
{
	public float x;
	public float y;
	public CityPrefabs obj;
	public float windIntensity;
	public WindDirection direction;
}

[System.Serializable]
public class LevelColors
{
	public Color background;
	public Color topbar;
	public Color ground;
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public GameObject m_casinoShop;


	public GameObject m_rootUIScore;
	public GameObject m_rootUIBestScore;
	public GameObject m_rootMain;

	public List<ShopBird> m_listShopBirds;
	public List<ShopBird> m_listShopBalls;

	public List<int> m_listUnlockProgress;

	public List<int> m_listItemsToUnlock;
	public int m_newBottlesToUnlock;

	public GameObject m_objectWatchVideo;
	public GameObject m_objectChangeModeBtn;

	//public GameObject m_objectCharHead;
	//public GameObject m_objectParticle;
	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_prefabEnemy;

	public GameObject m_prefabArrow;
	public GameObject m_prefabBird;
	public GameObject m_prefabBirdBig;
	public GameObject m_prefabBall;

	public GameObject m_objectTarget;
	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTutorialTitle;
	public GameObject m_objectTopBar;
	public GameObject m_objectModeIndicator;
	public GameObject m_objectHitParticle;
	public GameObject m_objectHitBow;
	public GameObject m_objectEarned;

	public GameObject m_objectWindManager;

	public GameObject m_objectTextTutorial;

	public GameObject m_objectRootArrows;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;

	public GameObject m_objectHeadshot;

	public GameObject m_objectBottle;

	public GameObject m_objectFullBar;

	public GameObject m_objectCoinRate;

	public List<GameObject> m_listButtons;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioDieBurn;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;
	public AudioClip m_audioScore;
	public AudioClip m_audioCoin;
	public AudioClip m_audioReward;

	public AudioClip m_audioThrow;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public List<Color> m_listTextMultiplierColor;

	public Text m_textScore;
	public Text m_textScoreShadow;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textModes2;
	public Text m_textTries2;
	public Text m_textBest;
	public Text m_textPerScore;

	public TextMesh m_textMeshCoin;
	public TextMesh m_textMeshCoinShadow;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public GameObject m_objectBall;
	public GameObject m_objectBallRoot;

	public GameObject m_objectTutorialArrow;

	public GAME_STATE m_eState;

	public GameObject m_objectCoin;

	public List<Image> m_listOfButtons;
	public List<Image> m_listOfButtonsResults;

	public List<int> m_listToRandomUnlock;

	public FDCharacter m_scriptCharacter;

	public PhysicsMaterial2D m_bottlePhysicMaterial;
	public GameObject m_objectCharacterRoot;

	public List<FDGamesceneInstance> m_listGamesceneInstance;

	public int m_birdType = 0;

	public ParticleSystem m_particleScore1;
	public ParticleSystem m_particleScore2;

	public GameObject m_objectClaimButton;
	public List<Vector3> m_listAchievementTuning; // Score, Number, Reward
	int m_currentAchievementLevel;
	int m_currentAchievementLevelCount;

	public Image m_imageAchievementBar;
	public Text m_textAchievementLevel;
	public Text m_textAchievementType;
	public Text m_textAchievementCount;

	public List<GameObject> m_listiPXAffectedObjects;

	FDGamesceneInstance m_scriptGamesceneInstance;

	int m_isWatchVideo;

	int m_currentHighscore;
	int m_currentLevel;
	int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	int m_currentMode;
	int m_currentBird;
	int m_currentPurchaseCount;
	int m_currentAttempts;
	int m_currentCoinAdd;
	public int m_currentFidgetObstacle;
	public int m_currentTotalCoinAdd;

	float m_previousPosition = 0;

	float m_currentIncentifiedAdsTime;
	float m_currentDirectionTime;

	float m_currentLeftGroupPosition = -999;
	float m_currentRightGroupPosition = -999;

	float m_currentAdsTime;

	bool m_isSuccess = false;

	string m_stringSuffix = "";

	bool m_animateIn = true;

	[Header("LAVA MODE : START")]
	[Space]

	public Material m_materialNormal;
	public Material m_materialLava;


	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Hit")) 
		{
			PlayerPrefs.SetInt ("Hit", 0);
		}//PlayerPrefs.SetInt ("Hit", 0);

		if (!PlayerPrefs.HasKey ("Mode")) 
		{
			PlayerPrefs.SetInt ("Mode", 0);
		}//PlayerPrefs.SetInt ("Mode", 0);

		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) 
		{	PlayerPrefs.SetInt ("currentBird", 0);
		}//PlayerPrefs.SetInt ("currentBird", 0);

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) 
		{	PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}//PlayerPrefs.SetInt ("currentPurchaseCount", 0);

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_onetries")) {
			PlayerPrefs.SetInt ("achievement_onetries", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_level")) {
			PlayerPrefs.SetInt ("achievement_level", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_level_count")) {
			PlayerPrefs.SetInt ("achievement_level_count", 0);
		}

		if (!PlayerPrefs.HasKey ("currentUnlockedBird")) {
			PlayerPrefs.SetInt ("currentUnlockedBird", 0);
		}
		//PlayerPrefs.SetInt ("currentUnlockedBird", 0);

		for (int x = 0; x < 36; x++) 
		{
//			PlayerPrefs.SetInt ("birdbought" + x, 0);
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 0);
				}
			}
			//PlayerPrefs.SetInt ("birdbought" + x, 0);
		}

		PlayerPrefs.SetInt ("birdbought0", 1);

		for (int x = 0; x < 10; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 0);
				}
			}
		}

		//PlayerPrefs.DeleteAll ();
		

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;

	}

	public GameObject m_objectFloorObject;
	public GameObject m_objectFloorLava;

	void Start()
	{
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 1);
		} 

		int f = 0;
		foreach (LeaderboardData data in ZGameMgr.instance.m_listLeaderboard) {
			if (!PlayerPrefs.HasKey (data.saveName)){
				if (m_listGamesceneInstance [f].m_isLevelBased)
					PlayerPrefs.SetInt (data.saveName, 1);
				else
					PlayerPrefs.SetInt (data.saveName, 0);
			}
			f++;
			//PlayerPrefs.SetInt (data.saveName, 1);
		}
			
		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}

		for (int x = 0; x < GameScene.instance.m_listModes.Count; x++) {
			if (!PlayerPrefs.HasKey ("Attempts" + x)) {
				PlayerPrefs.SetInt ("Attempts" + x, 0);
			}
		}

		Application.targetFrameRate = 60;

		ZPlatformCenterMgr.Instance.Login ();

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_currentBird = PlayerPrefs.GetInt ("currentBird");

		m_currentMode = PlayerPrefs.GetInt ("Mode");
		m_currentAttempts = PlayerPrefs.GetInt ("Attempts");
		m_currentIncentifiedAdsTime = 60;
		m_currentDirectionTime = 0.1f;

		m_currentCoinAdd = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		if (m_currentCoinAdd <= 0)
			m_currentCoinAdd = 1;

		m_currentAchievementLevel = PlayerPrefs.GetInt ("achievement_level");
		m_currentAchievementLevelCount = PlayerPrefs.GetInt ("achievement_level_count");

		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1);

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
		//m_textModes2.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name;// + " MODE";

		m_objectWatchVideo.SetActive (false);

		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];

		int gamesceneCount = 0;
		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}

		m_eState = GAME_STATE.START;

		#if UNITY_IPHONE
		bool deviceIsIphoneX = UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneX;
		if (deviceIsIphoneX) {
			// Do something for iPhone X
			foreach (GameObject obj in m_listiPXAffectedObjects) {
				obj.transform.localPosition -= new Vector3 (0, 25, 0);
			}
		}
		#endif

		m_previousScorePos = m_textScore.gameObject.transform.localPosition;
		m_previousShadowScorePos = m_textScoreShadow.gameObject.transform.localPosition;



		SetupLevel ();
		m_objectTextTutorial.SetActive (true);
		m_currentAds = 2;
		m_isWatchVideo = 0;

		ZAdsMgr.Instance.RequestInterstitial ();
	
		m_currentAdsTime = 35;

		//m_textScore.text = "";
		//m_textScoreShadow.text = "";



	}
		
	void Update()
	{
		if (ZGameMgr.instance.isPause)
			return;


		//if (m_eState == GAME_STATE.START || m_eState == GAME_STATE.SHOP)
		//	return;

		/*if (Input.GetMouseButtonDown (0)) {
			//m_scriptCharacter.Reset ();
			//m_scriptCharacter.Charge ();
			m_scriptGamesceneInstance.Gamescene_ButtonDown();
			foreach (Image obj in m_listOfButtons) {
				obj.gameObject.SetActive (false);
			}
			
		} else if (Input.GetMouseButtonUp (0)) {
			//m_scriptCharacter.Jump ();
			m_scriptGamesceneInstance.Gamescene_ButtonUp();
		}*/

		m_currentAdsTime -= Time.deltaTime;
	
		m_currentIncentifiedAdsTime -= Time.deltaTime;

		m_scriptGamesceneInstance.Gamescene_Update ();

		if(Application.isEditor && Input.GetMouseButtonDown(1)){
			ButtonDown();
		}
		if(Application.isEditor && Input.GetKeyDown(KeyCode.K)){
			ButtonDown();
		}
		if(Application.isEditor && Input.GetKeyDown(KeyCode.L)){
			ButtonDown();
		}
	}

	public void ButtonDown()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		
		m_scriptGamesceneInstance.Gamescene_ButtonDown();
		//foreach (Image obj in m_listOfButtons) {
			//obj.gameObject.SetActive (false);
		//}

		m_objectTopBar.SetActive (false);
		m_objectTextTutorial.SetActive (false);
		//m_objectCoinRate.SetActive (false);
		//m_objectModeIndicator.SetActive (false);

		if (m_textBest.gameObject.activeSelf) {
			//m_textBest.gameObject.SetActive (false);
			//m_textScore.text = m_currentScore + "/" + GetBestScore ();
			//m_textScoreShadow.text = m_currentScore + "/" + GetBestScore ();
			//m_textScore.text = "";
			//m_textScoreShadow.text = "";
			m_objectEarned.SetActive (false);

			m_objectCharTrail1.SetActive (true);
			m_objectCharTrail2.SetActive (true);
			m_objectNoInternet.SetActive (false);
		}
		m_objectWatchVideo.SetActive (false);

		m_eState = GAME_STATE.SHOOT;
	}

	public void ButtonUp()
	{
		m_scriptGamesceneInstance.Gamescene_ButtonUp();
	}

	public int GetCurrentMode()
	{
		return m_currentMode;
	}

	public void ClaimAchievement()
	{
		PlayerPrefs.SetInt ("achievement_level", PlayerPrefs.GetInt ("achievement_level") + 1);
		PlayerPrefs.SetInt ("achievement_level_count", 0);
		PlayerPrefs.Save ();

		m_currentAchievementLevel = PlayerPrefs.GetInt ("achievement_level");
		m_currentAchievementLevelCount = 0;

		AddCoins (Mathf.FloorToInt(m_listAchievementTuning [m_currentAchievementLevel].z));

		//UpdateAchievement (0);
	}

	void UpdateAchievement(int score)
	{
		//if (score >= m_listAchievementTuning [m_currentAchievementLevel].x) {
			//m_currentAchievementLevelCount++;
			//PlayerPrefs.SetInt ("achievement_level_count", m_currentAchievementLevelCount);
			//PlayerPrefs.Save ();
			//LeanTween.delayedCall(2.1f, UpdateAchievementUpgrade);
			//UpdateAchievementUpgrade();
		//}

		if (m_currentAchievementLevelCount >= m_listAchievementTuning [m_currentAchievementLevel].y) {
			//m_objectClaimButton.SetActive (true);
			//ClaimAchievement ();
		} else {
			//m_objectClaimButton.SetActive (false);
		}



		/*if (score <= m_listAchievementTuning [m_currentAchievementLevel].y) {
			m_imageAchievementBar.transform.localScale = new Vector3 (score * 1.0f / m_listAchievementTuning [m_currentAchievementLevel].y, 1, 1);
			m_imageAchievementBar.color = Color.white;
			m_textAchievementLevel.text = "LVL. " + (m_currentAchievementLevel+1);
			m_textAchievementType.text = "SCORE " ;//+ m_listAchievementTuning[m_currentAchievementLevel].x;
			m_textAchievementCount.text = "" + score + "/" + m_listAchievementTuning [m_currentAchievementLevel].y;
		} //else {
			//m_imageAchievementBar.color = Color.blue;
		//}*/

		//if (score == m_listAchievementTuning [m_currentAchievementLevel].y) {
		//	m_imageAchievementBar.color = Color.blue;
		//}
	}


	public GameObject m_objectLevelIndicator;

	void UpdateAchievementUpgrade()
	{
		ClaimAchievement ();
		//m_objectClaimButton.SetActive (true);
		m_currentAchievementLevelCount++;
		PlayerPrefs.SetInt ("achievement_level_count", m_currentAchievementLevelCount);
		PlayerPrefs.Save ();



		LeanTween.cancel (m_objectLevelIndicator.gameObject);
		//m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_objectLevelIndicator.gameObject, m_objectLevelIndicator.gameObject.transform.localScale + new Vector3 (0.4f, 0.4f, 0.4f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		//if (score == m_listAchievementTuning [m_currentAchievementLevel].y) {
			//m_imageAchievementBar.color = Color.green;
		//}
		//m_imageAchievementBar.transform.localScale = new Vector3 (m_currentAchievementLevelCount * 1.0f / m_listAchievementTuning [m_currentAchievementLevel].y, 1, 1);

		LeanTween.delayedCall (1f, UpdateAchievementUpgrade2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//LeanTween.delayedCall (0.2f, UpdateAchievementUpgrade2);
	}

	void UpdateAchievementUpgrade2()
	{
		m_textAchievementLevel.text = "LVL. " + (m_currentAchievementLevel+1);
		m_textAchievementType.text = "SCORE ";// + m_listAchievementTuning[m_currentAchievementLevel].x;


		LeanTween.cancel (m_textAchievementLevel.gameObject);
		//m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textAchievementLevel.gameObject, m_textAchievementLevel.gameObject.transform.localScale + new Vector3 (0.4f, 0.4f, 0.4f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		LeanTween.delayedCall (1f, UpdateAchievementUpgrade3);
	}

	void UpdateAchievementUpgrade3()
	{
		//m_imageAchievementBar.color = Color.white;
		//m_imageAchievementBar.color = Color.red;
		m_textAchievementCount.text = "" + 0 + "/" + m_listAchievementTuning [m_currentAchievementLevel].y;
		LeanTween.scale (m_imageAchievementBar.gameObject, new Vector3 (0 * 1.0f / m_listAchievementTuning [m_currentAchievementLevel].y, 1, 1), 0.01f).setEase(LeanTweenType.easeOutCubic);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	Vector3 m_previousScorePos;
	Vector3 m_previousShadowScorePos;

	bool m_isNewBest = false;

	public List<Color> m_listMessageColors;

	public void ShowMessage(string messageData, int type=0){
		m_textMiss.gameObject.SetActive (true);
		m_textMiss.text = messageData;
		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		m_textMiss.color = m_listMessageColors [type];
		LeanTween.delayedCall (1.5f, HideMessage);

		switch (type) {
		case 2:
			ZAudioMgr.Instance.PlaySFX (m_audioReward);
			break;
		}
	}

	void HideMessage()
	{
		m_textMiss.gameObject.SetActive (false);
	}

	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		//Debug.Log ("Die");

		m_textScore.color = m_colorTextMiss;
		m_objectMiss.SetActive (true);
		m_objectWatchVideo.SetActive (false);

		StopCoroutine ("whileTest");
		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentScore);

		int chatvalue = Random.Range (0, 15);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "Miss!";
			break;
		case 1 : 
			m_textMiss.text = "Woops!";
			break;
		case 2 : 
			m_textMiss.text = "Oh no!";
			break;
		case 3 : 
			m_textMiss.text = "No!";
			break;
		case 4 : 
			m_textMiss.text = "Argh!";
			break;
		case 5 : 
			m_textMiss.text = "Nooo!";
			break;
		case 6 : 
			m_textMiss.text = "Ow!";
			break;
		case 7:
			m_textMiss.text = "Too Bad!";
			break;
		case 8:
			m_textMiss.text = "D'oh!";
			break;
		case 9:
			m_textMiss.text = "Ouch!";
			break;
		case 10:
			m_textMiss.text = "Aww!";
			break;
		case 11:
			m_textMiss.text = "Almost!";
			break;
		case 12:
			m_textMiss.text = "Bam!";
			break;
		case 13:
			m_textMiss.text = "Pow!";
			break;
		default : 
			m_textMiss.text = "Miss!";
			break;
		}

		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		m_textMiss.color = m_listMessageColors [0];

		//ZCameraMgr.instance.gameObject.transform.position = m_scriptCharacter.gameObject.transform.position;

		//LeanTween.move (ZCameraMgr.instance.gameObject, new Vector3(m_scriptCharacter.gameObject.transform.position.x + 8f, ZCameraMgr.instance.gameObject.transform.position.y, ZCameraMgr.instance.gameObject.transform.position.z), 0.2f);
		//ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 80;
		//eanTween.cancel (ZCameraMgr.instance.gameObject);
		//ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (39.57f, -0.6f, -254);


		//m_textBest.gameObject.SetActive (true);
		m_scriptGamesceneInstance.Gamescene_Die();

		//if (m_currentScore == 0) {
		//	m_textScore.text = "" + m_currentScore;
		//	m_textScoreShadow.text = "" + m_currentScore;
		//}




		//m_textScore.text = m_currentScore + "";// + "/" + GetBestScore ();
		//m_textScoreShadow.text = m_currentScore + "";// "/" + GetBestScore ();



		ZCameraMgr.instance.DoShake ();

//		if (m_currentMode == 14) {
//			ZAudioMgr.Instance.PlaySFX (m_audioDieBurn);
//		} else {
		//ZAudioMgr.Instance.PlaySFX (m_audioDie, Random.Range(0.8f, 1.2f));
//		}
	
		m_eState = GAME_STATE.RESULTS;
	
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
			PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
			PlayerPrefs.Save ();

			m_currentLevel = m_currentScore;
			m_isNewBest = true;

			// Only submit if new high score
			#if UNITY_ANDROID
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
			#else
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
			#endif
		}



		//UpdateAchievement (m_currentScore);


		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel - m_currentScore);

		
		m_currentAds++;



		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);
		
		// Achievment perseverance
		if (m_currentAttempts >= 100) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEQ");
		}

		if (m_scriptGamesceneInstance.m_isLevelBased) 
		{
			int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
			PlayerPrefs.SetInt ("Attempts" + m_currentMode, currentAttempts + 1);
			//m_textTries2.color = m_colorTextCorrect;
			if( m_currentAttempts == 0 )
				m_textTries2.text = "";
			else
				m_textTries2.text = "TRIES " + (currentAttempts + 1);
		} 
		else if (m_currentMode == 14) 
		{
			m_objectBottle.transform.parent = null;
			m_objectCamera.GetComponent<CameraLavaMode> ().StopLava ();
			//m_objectFloorObject.GetComponent<FloorObject> ().FloorIsRising (false);
		}
		else 
		{
			m_textTries2.text = "";
		}

		m_objectCharTrail1.GetComponent<TrailRenderer> ().time = 0.2f;
		m_objectCharTrail2.GetComponent<TrailRenderer> ().time = 0.2f;

		//if (m_currentHits >= m_listPrices[m_currentPurchaseCount]) {
			//m_currentAchievementLevelCount++;
			//PlayerPrefs.SetInt ("achievement_level_count", m_currentAchievementLevelCount);
			//PlayerPrefs.Save ();
			//LeanTween.delayedCall(2.1f, UpdateAchievementUpgrade);
			//LeanTween.delayedCall(1.5f, UpdateAchievementUpgrade );

			//LeanTween.delayedCall (3f, ShowResultsScreen);
			//m_objectReadyUnlock.SetActive (true);
			//LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		//	LeanTween.delayedCall (2f, ShowResultsScreen);
			//LeanTween.delayedCall(2.5f, ShowUnlock);
			//ShowUnlock();
		//} else {	
		LeanTween.delayedCall (1f, ShowResultsScreen);
		//}
	}

	void DieRoutine()
	{
		//m_textScoreShadow.gameObject.SetActive(false;l

		//LeanTween.moveLocal(m_textScore.gameObject, m_textScore.gameObject.transform.localPosition + new Vector3(84f, -9f, 0), 0.1f);
		//LeanTween.moveLocal(m_textScoreShadow.gameObject, m_textScore.gameObject.transform.localPosition + new Vector3(84f, -9f, 0), 0.1f);
		//LeanTween.scale (m_textScore.gameObject, new Vector3 (1.2f, 1.2f, 0.8f), 0.1f);
		//LeanTween.scale (m_textScoreShadow.gameObject, new Vector3 (1.2f, 1.2f, 0.8f), 0.1f);

		m_objectCharTrail1.GetComponent<TrailRenderer> ().time = 0f;
		m_objectCharTrail2.GetComponent<TrailRenderer> ().time = 0f;

		ZAudioMgr.Instance.PlaySFX(m_audioButton);

		LeanTween.delayedCall (0.5f, ShowResultsScreen);
	}

	public ShopBird GetCurrentShopBird()
	{
		return m_listShopBirds [GetCurrentSelectedBird ()];
	}

	public int GetCurrentCoinAdd()
	{
		return m_currentCoinAdd;
	}

	void ShowUpgradeCoin()
	{
		m_textPerScore.gameObject.SetActive (true);
		m_textPerScore.text = "+" + GetCurrentCoinAdd () + "/SCORE";

		Vector3 currentPosition = m_textPerScore.gameObject.transform.localPosition;
		m_textPerScore.gameObject.transform.localPosition += new Vector3 (0, 20, 0);
		LeanTween.moveLocal (m_textPerScore.gameObject, currentPosition, 0.2f);

		m_currentCoinAdd = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		if (m_currentCoinAdd <= 0)
			m_currentCoinAdd = 1;

		LeanTween.delayedCall (1f, ShowUpgradeCoin2);

		ZAudioMgr.Instance.PlaySFX(m_audioButton);
	}

	void ShowUpgradeCoin2()
	{
		m_textPerScore.text = "+" + GetBestScore () + "/SCORE";

		LeanTween.cancel (m_textPerScore.gameObject);
		m_textPerScore.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
		LeanTween.scale (m_textPerScore.gameObject, m_textPerScore.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		LeanTween.delayedCall (2f, SetupLevel);

		ZAudioMgr.Instance.PlaySFX(m_audioReward);
	}

	void ShowResultsScreen()
	{
		/*if (m_isNewBest) {
			ShowUpgradeCoin ();
			m_isNewBest = false;
		} else */if (m_currentHits >= m_listPrices [m_currentPurchaseCount] && PlayerPrefs.GetInt ("currentPurchaseCount") < m_listShopBirds.Count-1) {
			//m_currentAchievementLevelCount++;
			//PlayerPrefs.SetInt ("achievement_level_count", m_currentAchievementLevelCount);
			//PlayerPrefs.Save ();
			//LeanTween.delayedCall(2.1f, UpdateAchievementUpgrade);
			//LeanTween.delayedCall(1.5f, UpdateAchievementUpgrade );

			//LeanTween.delayedCall (3f, ShowResultsScreen);
			ZAudioMgr.Instance.PlaySFX(m_audioReward);
			m_objectReadyUnlock.SetActive (true);
			LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.1f, 0.1f, 0.1f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);

			LeanTween.delayedCall (2f, ShowUnlock);
			//Debug.Log ("Results : Show Unlock");
			//ShowUnlock();
		} else if (m_currentLevel > 3 && m_currentAds > 7 && m_currentAdsTime <= 0 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
			m_currentAds = 0;
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAdsTime = 60;
			LeanTween.delayedCall (1f, SetupLevel);
			//Debug.Log ("Results : Show Interestitial");
		} else if (m_currentLevel > 3 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			ZRewardsMgr.instance.ShowRewardSuccess ();
			ZAudioMgr.Instance.PlaySFX(m_audioReward);
			LeanTween.delayedCall (1.5f, SetupLevel);
			//Debug.Log ("Results : Show Reward");
		} else {
			SetupLevel ();
			//Debug.Log ("Results : Setup Level");
		}
		/*return;

		if (m_currentLevel > 3 && m_currentAds > 7 && m_currentAdsTime <= 0 && ZAdsMgr.Instance.IsInterstitialAvailable ()) {
			m_currentAds = 0;
			ZAdsMgr.Instance.ShowInsterstitial ();
			m_currentAdsTime = 50;
			LeanTween.delayedCall (1f, ShowResultsScrenButtons);
		} else if (m_currentLevel > 3 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			ZRewardsMgr.instance.ShowRewardSuccess ();
			LeanTween.delayedCall (1f, ShowResultsScrenButtons);
		} else {
			ShowResultsScrenButtons ();
		}*/


	}

	void ShowResultsScrenButtons()
	{
		Vector3 currentPosition;
		foreach (Image buttonResults in m_listOfButtonsResults) {
			buttonResults.gameObject.SetActive (true);
			currentPosition = buttonResults.gameObject.transform.localPosition;
			buttonResults.gameObject.transform.localPosition -= new Vector3 (0, 50, 0);
			LeanTween.moveLocal (buttonResults.gameObject, currentPosition, 0.3f).setEase(LeanTweenType.easeOutCubic);
		}
	}

	public void Retry()
	{
		SetupLevel ();
	}

	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable()
	{
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		LeanTween.delayedCall (1.505f, SetupLevel);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	//	GameScene.instance.m_eState = GAME_STATE.SHOP;
	}

	void ShowReward(){
		LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}


	public int GetMode()
	{
		return m_currentMode;
	}

	public GameObject m_objectCamera;

	public void ResetCamera()
	{
		//LeanTween.move (ZCameraMgr.instance.gameObject, new Vector3 (m_scriptCharacter.gameObject.transform.position.x + 9f, 19f, -36.83f), 1f);
		//LeanTween.move (ZCameraMgr.instance.gameObject, new Vector3 (m_scriptCharacter.gameObject.transform.position.x + 12f, 35.1f, -90f), 0.7f);

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 140;
		LeanTween.cancel (ZCameraMgr.instance.gameObject);
		ZCameraMgr.instance.gameObject.transform.position = new Vector3 (m_scriptCharacter.gameObject.transform.position.x - 42f, 36.78f, -254f);
	}

	public void Score(int score)
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		
		m_currentScore += score;
		m_textScore.text = "" + m_currentScore;
		m_textScoreShadow.text = "" + m_currentScore;
		m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));


		m_scriptGamesceneInstance.Gamescene_Score ();
		//ResetCamera ();
		ZAudioMgr.Instance.PlaySFX (m_audioScore);

		//AddCoins (score);
		int multiplierIndex = Mathf.FloorToInt(Mathf.FloorToInt(GetScore() / 10f)) + 1;
		if (multiplierIndex >= 5)
			multiplierIndex = 5;
		multiplierIndex-=1;
		
		//AddCoins (GetCurrentCoinAdd() * multiplierIndex);
		//AddCoins (multiplierIndex);

		/*if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) 
		{
			PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
			PlayerPrefs.Save ();
			m_currentLevel = m_currentScore;
		}*/

		// WITH BEST SCORE
		//m_textScore.text = m_currentScore + "/" + GetBestScore ();
		//m_textScoreShadow.text = m_currentScore + "/" + GetBestScore ();
		m_textScore.text = m_currentScore + "";
		m_textScoreShadow.text = m_currentScore + "";

		if (m_scriptGamesceneInstance.m_isLevelBased) {
			Success ();
		}

		//m_objectParticlePerfect.GetComponent<ParticleSystem> ().Clear ();
		//m_objectParticlePerfect.transform.position = WaterBottle.Instance.gameObject.transform.position;
		//m_objectParticlePerfect.SetActive (true);
		//m_objectParticlePerfect.GetComponent<ParticleSystem> ().Play ();
		//UpdateAchievement(m_currentScore);
	}
		
	private void CheckAchievementStatus()
	{
		// Current level achievement one tries
		if (m_currentAttempts == 1) {
			PlayerPrefs.SetInt ("achievement_onetries", PlayerPrefs.GetInt ("achievement_onetries") + 1);
		} else {
			PlayerPrefs.SetInt ("achievement_onetries", 0);
		}

		if (PlayerPrefs.GetInt ("achievement_onetries") == 5) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEA");
		}

		// Current level achievement section
		if (m_currentLevel == 100) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAQ");
		} else if (m_currentLevel == 500) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAg");
		} else if (m_currentLevel == 1000) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAw");
		} else if (m_currentLevel == 2500) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQBA");
		} else if (m_currentLevel == 5000) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQBQ");
		}
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		m_currentAds++;

		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else
		/*if (m_currentMode == 0) {
		ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_IOS);
		}
		else if (m_currentMode == 1) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "highscore_leaderboard_quick");
		}
		else if (m_currentMode == 2) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "highscore_leaderboard_endless");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

		m_objectMiss.SetActive (true);

		int chatvalue = Random.Range (0, 15);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "YEY!";
			break;
		default : 
			m_textMiss.text = "WOOT!";
			break;
		}

		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (6).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		m_textMiss.color = m_listMessageColors [1];
			
		LeanTween.delayedCall (1.5f, Success2);

		m_eState = GAME_STATE.RESULTS;

		if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
			PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
			PlayerPrefs.Save ();

			m_currentLevel = m_currentScore;
			m_isNewBest = true;

			// Only submit if new high score
			#if UNITY_ANDROID
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
			#else
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
			#endif
		}

	}

	void Success2()
	{
		SetupLevel ();
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;
		
		//if( isRateUs > 0 )
		//	return;
		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			//m_currentAds = 0;
		} else {
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
				ContinueLevel ();
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		//m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public int GetLevel()
	{
		if (m_currentMode == 2 || m_currentMode == 12 || m_currentMode == 14) 
		{
			return m_currentScore;
		} else {
			return m_currentLevel;
		}
	}

	public void Play()
	{	
		if (m_currentMode == 1 || m_currentMode == 15)
		{
			m_currentHighscore = m_currentLevel;
		}


		m_eState = GAME_STATE.IDLE;
		m_textScore.color = m_colorTextCorrect;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

		m_textScore.color = m_colorTextCorrect;
		m_eState = GAME_STATE.IDLE;

	}

	bool isChangeModeFromButton = false;

	public void ChangeMode(int p_modeSelected)
	{
		//Debug.LogError ("Change Mode");

		isChangeModeFromButton = true;

		m_currentMode++;// = p_modeSelected;
		if (m_currentMode >= m_listGamesceneInstance.Count)
			m_currentMode = 0;

		int gamesceneCount = 0;
		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}
		m_listGamesceneInstance [m_currentMode].gameObject.SetActive (true);

		//Debug.Log ("Current Mode : " + m_currentMode);

		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
		//m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";


		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];
		m_scriptGamesceneInstance.Gamescene_Setuplevel (true);


		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		isChangeModeFromButton = false;



		SetupLevel ();

		CloseModeSelect ();
	}

	public void ChangeMode()
	{
		

		isChangeModeFromButton = true;
		m_currentMode++;
		if (m_currentMode >= m_listGamesceneInstance.Count) 
		{
			m_currentMode = 0;
		}

		int gamesceneCount = 0;
		foreach (FDGamesceneInstance instance in m_listGamesceneInstance) {
			if (gamesceneCount != m_currentMode) {
				instance.gameObject.SetActive (false);
			} else {
				instance.gameObject.SetActive (true);
			}
			gamesceneCount++;
		}
		m_listGamesceneInstance [m_currentMode].gameObject.SetActive (true);




		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name;// + " MODE";

		

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		//Debug.LogError ("Change Mode : " + m_currentMode);

		m_scriptGamesceneInstance.Gamescene_Unloadlevel ();
		m_scriptGamesceneInstance = m_listGamesceneInstance [m_currentMode];


		SetupLevel ();
		m_scriptGamesceneInstance.Gamescene_Setuplevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		isChangeModeFromButton = false;
	}

	void ResetModeText()
	{
		if (m_currentMode == 0 || m_currentMode == 16) {
			m_textLevel.text = "LEVEL";
		} else if (m_currentMode == 1 || m_currentMode == 11 || m_currentMode == 13 || m_currentMode == 15) {
			m_textLevel.text = "Highscore";
		}
		else if (m_currentMode == 2 || m_currentMode == 5 || m_currentMode == 6 || m_currentMode == 8 || m_currentMode == 12  || m_currentMode == 14) 
		{
			m_textLevel.text = "Highscore";
		} 
		else if (m_currentMode == 9) 
		{
			if (m_currentMode == 9 && m_currentChallengeModeTries == 0) 
			{
					m_textLevel.text = "Highscore";
			} 
			else 
			{
				m_textLevel.text = "Score";
			}
		}
		else
			m_textLevel.text = "LEVEL";
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		ScreenCapture.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		//ShareScreenshotWithText ("");
		//ZFollowUsMgr.Instance.ShareScreenshotWithText ();
		#if UNITY_ANDROID
		if (UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)) {
			ZFollowUsMgr.Instance.Share ();
			ZAnalytics.Instance.SendShareButton ();
		} else {
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			}, () => {
			// add not permit action
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			});
		}
		#else
		ZFollowUsMgr.Instance.Share ();
		ZAnalytics.Instance.SendShareButton ();
		#endif
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		//ZPlatformCenterMgr.Instance.Login ();
		//return;
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.PostScore(10);
		//return;

		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;
	public GameObject m_objectUnlock;
	public GameObject m_objectModeSelect;

	public void ShowModeSelect()
	{
		if (m_objectShop.gameObject.activeSelf) 
		{
			m_objectShop.gameObject.SetActive (false);
		}

		m_objectModeSelect.gameObject.SetActive (true);
		m_eState = GAME_STATE.START;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//mouseDown = false;
	}

	public void CloseModeSelect()
	{
		m_eState = GAME_STATE.IDLE;
		//m_objectModeSelect.gameObject.SetActive (false);
		GameScene.instance.m_rootUIScore.gameObject.SetActive (true);
		GameScene.instance.m_rootUIBestScore.gameObject.SetActive (true);
		GameScene.instance.m_rootMain.gameObject.SetActive (true);
		GameScene.instance.SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowShop(bool p_isUnlock = false)
	{
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 60;
		LeanTween.cancel (ZCameraMgr.instance.gameObject);
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (-44.65f, -0.3f, -254);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3(7f, 10f, 0);

		m_rootUIScore.gameObject.SetActive (false);
		m_rootUIBestScore.gameObject.SetActive (false);
		m_rootMain.gameObject.SetActive (false);

		m_objectShop.gameObject.SetActive (true);
		ShopScene.instance.SetupScene(p_isUnlock);


//		m_objectShop.SetActive(true);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		
		ZAnalytics.Instance.SendShopButton ();

		m_eState = GAME_STATE.SHOP;
		//mouseDown = false;

//		LeanTween.delayedCall (0.1f, ShowDefaultLight);

		Debug.Log ("TEST");
	}

//	private void ShowDefaultLight()
//	{
//		m_directionalLight.gameObject.SetActive (true);
//		m_directionalLavaLight.gameObject.SetActive (false);
//	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index, bool playSound=false)
	{
		//if (index <= 0)
		//	index = 1;

		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);

		m_currentTotalCoinAdd += index;
	
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		//if (playSound) {
			ZAudioMgr.Instance.PlaySFX (m_audioCoin);
		//}

		if (index <= 0) {
			m_textMeshCoin.text = "";
			m_textMeshCoinShadow.text = "";
		} else {
			m_textMeshCoin.text = "+" + index;
			m_textMeshCoinShadow.text = "+" + index;
		}

		if( index > 5 ) {
		LeanTween.scale (m_objectFullBar.gameObject, new Vector3 (Mathf.Min (1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1), 0.6f).setEase (LeanTweenType.easeInCubic).setOnComplete(AddCoinPlayAudioFull);
		ZAudioMgr.Instance.PlaySFX (m_audioAddCoinGain);
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarGain;
		} else {
			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1);
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarGain;
		LeanTween.delayedCall (0.2f, AddCoinRoutine3);
		}



		/*if (m_currentHits >= GetCurrentShopPrice () && m_eState == GAME_STATE.START && m_currentPurchaseCount < m_listShopBirds.Count) {
			LeanTween.delayedCall (0.2f, AddCoinRoutine);
		}*/

	}

		public Color m_colorFullBarYellow;
		public Color m_colorFullBarGain;
		public Color m_colorFullBarFull;

		public AudioClip m_audioAddCoinGain;
		public AudioClip m_audioAddCoinFull;

		void AddCoinPlayAudioFull()
		{
		ZAudioMgr.Instance.PlaySFX (m_audioAddCoinFull);
		if (GetCurrentCoins() >= GetCurrentShopPrice ()) {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
		} else {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
		}
		}
	void AddCoinRoutine3()
		{
		if (GetCurrentCoins() >= GetCurrentShopPrice ()) {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarFull;
		} else {
		m_objectFullBar.GetComponent<Image>().color = m_colorFullBarYellow;
		}
		//m_directionalLight.intensity = 0.7f;
		//	m_spotLight.SetActive (false);
	}

		public Color m_colorCOinYellow;

	public void SpendCoins(int index){
		m_currentHits -= index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);

		m_currentPurchaseCount++;

		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		//if (playSound) {
		ZAudioMgr.Instance.PlaySFX (m_audioCoin);
		//}

		m_textMeshCoin.text = "+" + index;
		m_textMeshCoinShadow.text = "+" + index;

		m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1);

		m_objectFullBar.GetComponent<Image> ().color = m_colorCOinYellow;
	}

		public void ResetCoins()
		{
			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1);
		}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		//return;

		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public int GetCurrentSelectedBird()
	{
		return m_currentBird;
	}

	public List<GameObject> m_listBirds;

		public Text m_textName;

		public ShopBird m_currentBirdObject;
	public GameObject m_objectCharTrail1;
	public GameObject m_objectCharTrail2;
	public GameObject m_objectCharParticle;

	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0)
		{
			m_currentBird = count;
			PlayerPrefs.SetInt ("currentBird", m_currentBird);
			//m_objectShop.SetActive (false);
			//m_casinoShop.SetActive (false);


		
			SetupBird ();
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);

			ShopScene.instance.SelectBird ();
			//m_rootUIScore.gameObject.SetActive (true);
			//m_rootMain.gameObject.SetActive (true);
			//SetupLevel ();
			m_eState = GAME_STATE.IDLE;

			Debug.Log ("Use Bought UseBird");
		} 
		else 
		{
			//BuyBird (count);
			//ZAudioMgr.Instance.PlaySFX (m_audioBuy);
			Debug.Log ("Use Not Bought UseBird");
		}


	}

	void SetupBird()
	{
		int x = 0;
		foreach (ShopBird objBird in m_listShopBirds) 
		{
			if (x == m_currentBird) 
			{
				objBird.objectCharacter.SetActive (true);
				m_textName.text = objBird.m_textCharacterName;

		m_objectCharTrail1.GetComponent<TrailRenderer> ().startColor = objBird.colorTrailStart;
		m_objectCharTrail2.GetComponent<TrailRenderer> ().startColor = objBird.colorTrailStart;
		m_objectCharTrail1.GetComponent<TrailRenderer> ().endColor = objBird.colorTrailEnd;
		m_objectCharTrail2.GetComponent<TrailRenderer> ().endColor = objBird.colorTrailEnd;

				ParticleSystem.MainModule mainColor = m_objectCharParticle.GetComponent<ParticleSystem> ().main;
				mainColor.startColor = objBird.colorFeathers;
			} else {
				objBird.objectCharacter.SetActive (false);
			}
			x++;
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}

		public GameObject m_objectBox;
		public GameObject m_objectTree;
		//public GameObject m_objectPlay;

	public void ShowUnlock()
	{
		// Set Camera
		// new Vector3(-6.5f, 6.7f, -254)
		// ortho = 80

		//SetupLevel ();

		m_scriptGamesceneInstance.Gamescene_Setuplevel (true);

		ShowShop ();

		m_objectTree.SetActive (false);
		m_objectBox.SetActive (true);
		///m_objectPlay.SetActive (false);
		Vector3 currentPosition;
		currentPosition = m_objectBox.gameObject.transform.localPosition;
		m_objectBox.gameObject.transform.localPosition += new Vector3 (0, 30, 0);
		LeanTween.moveLocal (m_objectBox.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);

		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = 80;
		LeanTween.cancel (ZCameraMgr.instance.gameObject);
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().transform.localPosition = new Vector3 (41.79f, -10.7f, -254);
		ZCameraMgr.instance.transform.localEulerAngles = new Vector3(7f, 10f, 0);

		ZAudioMgr.Instance.PlaySFX (m_audioThrow);
		// Hide Level
		// Show Box

		m_eState = GAME_STATE.SHOP;
	}

		public int GetCurrentCoins()
		{
		return m_currentHits;
		}

	public void BuyBird(int count)
	{
		//if (m_currentHits >= m_listPrices[m_currentPurchaseCount] && PlayerPrefs.GetInt("birdbought" + count) == 0) 
		{
			m_eState = GAME_STATE.SHOP;
			PlayerPrefs.SetInt ("birdbought" + count, 1);
			
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();
			UseBird (count);

			m_objectFullBar.gameObject.transform.localScale = new Vector3 (Mathf.Min(1, m_currentHits * 1.0f / GetCurrentShopPrice ()), 1, 1);
			//ZUnlockManager.Instance.ShowUnlock(count, m_listPrices[m_currentPurchaseCount]);
			//m_objectShop.gameObject.SetActive (false);
			
		}
	}

	public void UnlockNewItem()
	{
		int count = 0;
		int unlokedControlledRandom = Random.Range (0, 100);

		m_listItemsToUnlock.Clear ();
		m_newBottlesToUnlock = 6;

		m_objectChangeModeBtn.gameObject.SetActive (false);
		m_eState = GAME_STATE.SHOP;

		for (int x = 0; x < m_listShopBirds.Count; x++) 
		{
			if (PlayerPrefs.GetInt ("birdbought" + x) > 0) {

				if (x > 30) 
				{
					m_newBottlesToUnlock -= 1;
				}
			}
			else 
			{
				m_listItemsToUnlock.Add(x);
			}
		}

		// + EDIT JOSH 20170706
		// Controlled random unlock
		if (unlokedControlledRandom > 30) 
		{
		// + DAM - the algo is not so controlled, just making it full random for now
		//int randomval = Random.Range (m_listItemsToUnlock.Count - m_newBottlesToUnlock - 1, m_listItemsToUnlock.Count - 1);
		int randomval = Random.Range (0, m_listItemsToUnlock.Count - 1);
		int itemId = m_listItemsToUnlock [randomval];
		ZUnlockManager.Instance.ShowUnlock(itemId, m_listPrices[m_currentPurchaseCount]);
		} 
		else 
		{
			int randomval = Random.Range (0, m_listItemsToUnlock.Count - 1);
			int itemId = m_listItemsToUnlock [randomval];

		ZUnlockManager.Instance.ShowUnlock(itemId, m_listPrices[m_currentPurchaseCount]);
		}
	}

	public void PurchaseItem()
	{
		m_currentHits -= m_listPrices [m_currentPurchaseCount];
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	}

	public void ConfirmCasinoPurchase(int count)
	{
		PlayerPrefs.SetInt ("birdbought" + count, 1);

		ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
		ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);

		m_currentPurchaseCount++;
		PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
		PlayerPrefs.Save ();

		//UseBird(count);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//ZAudioMgr.Instance.PlaySFX (m_audioButton);

		//CasinoShop.instance.SetupScene (true, true);
		//ShopScene.instance.SetupScene (true, true);

		int x = 0;
		int currentBirdCount = 0;
		foreach (ShopBird objBird in m_listShopBirds) {
		if (PlayerPrefs.GetInt ("birdbought" + x) == 1) 
		{
			currentBirdCount++;
		}
			x++;
		}

		if (currentBirdCount >= 15) 
		{
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEg");
		}
	}

	public int GetBestScore()
	{
		return PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);
	}

	public int GetScore()
	{
		return m_currentScore;
	}

	public void ConfirmBirdPurchase(int count)
	{
		m_currentHits -= m_listPrices[m_currentPurchaseCount];
		PlayerPrefs.SetInt ("Hit", m_currentHits);
		PlayerPrefs.SetInt ("birdbought" + count, 1);

		ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
		ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);

		m_currentPurchaseCount++;
		PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
		PlayerPrefs.Save ();

		//UseBird(count);
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		//ZAudioMgr.Instance.PlaySFX (m_audioButton);

		//ShopScene.instance.SetupScene (true, true);

		int x = 0;
		int currentBirdCount = 0;
		foreach (ShopBird objBird in m_listShopBirds) {
		if (PlayerPrefs.GetInt ("birdbought" + x) == 1) 
		{
			currentBirdCount++;
		}
			x++;
		}

		if (currentBirdCount >= 15) 
		{
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEg");
		}
	}

	public BoxCollider2D m_bottleCollider;
	public BoxCollider2D m_bottleSideCollider;
	public BoxCollider2D m_bottleEndCollider;

	public void OpenAchievements()
	{
		//UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE);
		/*UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
		// add permit action
		}, () => {
			// add not permit action
		});*/

		ZPlatformCenterMgr.Instance.OpenAchievements ();
	}

	public void BuyBall(int count)
	{
		if (m_currentHits >= 250  && PlayerPrefs.GetInt("ballbought" + count) == 0) {
			m_currentHits -= 250;
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("ballbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("ballbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("ball", m_currentPurchaseCount);

			//m_currentPurchaseCount++;
			//PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			//PlayerPrefs.Save ();

			//UseBird(count);
			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	//ZAudioMgr.Instance.PlaySFX (m_audioButton);randomVerticalPosition

			ShopScene.instance.SetupScene ();
		}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void RefreshMode()
	{	//PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);
		/*if( m_currentMode == 0 )
			m_currentLevel = PlayerPrefs.GetInt ("Level");
			//m_currentLevel = 60;
		else if( m_currentMode == 1 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelQuick");
			//m_currentLevel = 75;
		else if( m_currentMode == 2 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelEndless");
			//m_currentLevel = 75;*/

		if (m_scriptGamesceneInstance.m_isLevelBased) 
		{
			//if (m_currentChallengeModeTries == 0) 
			//{
				//m_currentLevel = 0;
			//}
			m_currentLevel = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		} 
//		else if (m_currentMode == 14) 
//		{
//			m_currentLevel = 0;
//		}
		else 
		{
			m_currentLevel = 0;//PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		}

		/*if (m_currentMode == 3) {
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
	} else {SetupLevel(
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
		}*/
		//m_objectTutorial.GetComponent<Text> ().text = "TAP TO PLAY";
		ResetScore ();
	}

	int m_currentTotalChallengeModeTries = 30;
	int m_currentChallengeModeTries = 0;
	

	void ShowTutorialRoutine()
	{
		if (m_eState != GAME_STATE.IDLE)
			return;

		//m_objectTextTutorial.SetActive (true);
		Vector3 currentPosition;
		currentPosition = m_objectTextTutorial.gameObject.transform.localPosition;
		m_objectTextTutorial.gameObject.transform.localPosition -= new Vector3 (0, 30, 0);
		LeanTween.moveLocal (m_objectTextTutorial.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);
	}

	//public void UpdateCharColors()
	//{
	//}

	public void SetupLevel()
	{
		//return;
		ZObjectMgr.Instance.ResetAll ();

		RefreshMode ();
		ResetModeText ();

		SetupBird ();
		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);
		m_objectHeadshot.SetActive (false);
		//m_objectParticle.SetActive (false);
		m_objectTextTutorial.SetActive (false);
		//m_objectModeIndicator.SetActive (true);


		m_objectCharTrail1.SetActive (false);
		m_objectCharTrail2.SetActive (false);

		m_objectCharTrail1.GetComponent<TrailRenderer> ().time = 1f;
		m_objectCharTrail2.GetComponent<TrailRenderer> ().time = 1f;

		m_objectTree.SetActive (true);
		m_objectBox.SetActive (false);

		//m_objectCoinRate.SetActive (true);

		m_textBest.gameObject.SetActive (true);

		m_textBest.text = "LEVEL";// + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName);

		//UpdateLevelColors ();

		/*foreach (Image buttonResults in m_listOfButtonsResults) {
		buttonResults.gameObject.SetActive (false);
		}*/

		/*Vector3 currentPosition;
		int buttonIndex = 0;
		foreach (Image obj in m_listOfButtons) {
			
			obj.gameObject.SetActive (true);
			if (buttonIndex != 2) {
				currentPosition = obj.gameObject.transform.localPosition;
				obj.gameObject.transform.localPosition -= new Vector3 (0, 50, 0);
				LeanTween.moveLocal (obj.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);
			}
			buttonIndex++;
		}*/

		m_objectTopBar.SetActive (true);
		//currentPosition = m_objectTopBar.gameObject.transform.localPosition;
		//m_objectTopBar.gameObject.transform.localPosition += new Vector3 (0, 20f, 0);
		//LeanTween.moveLocal (m_objectTopBar.gameObject, currentPosition, 0.3f).setEase (LeanTweenType.easeOutCubic);

		LeanTween.delayedCall (0.3f, ShowTutorialRoutine);

		m_textScore.gameObject.transform.localPosition = m_previousScorePos;
		//m_textScore.gameObject.transform.localPosition = m_previousShadowScorePos;
		//m_textScore.gameObject.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
		m_textScore.gameObject.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);

		m_textPerScore.text = "+" + GetCurrentCoinAdd () + "/SCORE";
		m_currentTotalCoinAdd = 0;

		/*LeanTween.moveLocal(m_textScore.gameObject, m_previousScorePos, 0.1f);
		LeanTween.moveLocal(m_textScoreShadow.gameObject, m_previousShadowScorePos, 0.1f);
		LeanTween.scale(m_textScore.gameObject, new Vector3(0.8f, 0.8f, 0.8f), 0.1f);
		LeanTween.scale(m_textScoreShadow.gameObject, new Vector3(0.8f, 0.8f, 0.8f), 0.1f);*/


		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		m_scriptGamesceneInstance.Gamescene_Setuplevel (true);

		Debug.LogWarning ("Setup Level");

		int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
		PlayerPrefs.SetInt ("Attempts" + m_currentMode, currentAttempts + 1);
		m_textTries2.text = "TRIES " + (currentAttempts + 1);
		//m_currentSpawnCount = 0;

		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		PlayerPrefs.Save ();

		m_eState = GAME_STATE.IDLE;
		//SetupBird ();

		UpdateAchievement (0);

		//m_textScore.text = "";
		//m_textScoreShadow.text = "";

		m_textScore.gameObject.SetActive (true);
		m_textScoreShadow.gameObject.SetActive (true);

		m_textScore.text =  "" + GetBestScore ();
		m_textScoreShadow.text =  "" + GetBestScore ();

	}
		
	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		//m_currentSpawnCount = 0;
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		m_textHit.gameObject.SetActive (true);

		//m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);

		m_objectTutorial.SetActive (true);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_animateIn = true;
		m_eState = GAME_STATE.START;
	}

	void ResetScore()
	{
		m_textLevel.text = "Level";
		m_currentScore = m_currentLevel;
		if (m_currentMode == 9) {
			if (m_currentChallengeModeTries == 0)
				m_textScore.text = "" + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		} 
		else if (m_currentMode == 14) 
		{
			m_textScore.text = "" + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		}
		else 
		{
			//m_textScore.text = "" + m_currentScore;
			//m_textScoreShadow.text = "" + m_currentScore;
		}
	}


}
