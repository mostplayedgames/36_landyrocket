﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FDShootyEnum
{
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE
}

public class FDShootyBasket : FDGamesceneInstance {

	public static FDShootyBasket instance;
	public GameObject m_rootRocket;
	public Rigidbody2D m_rigidbodyRocket;
	public GameObject m_rootBasket;
	public List<GameObject> m_listBaskets;

	float m_currentMousePos;
	float m_currentRocketRotation;

	public FDShootyEnum m_eState;
	public int m_currentScoreIndex = 0;

	public AudioClip m_audioBall;
	public AudioClip m_audioShoot;

	public List<Vector3> m_listTuning;
	public List<Vector2> m_listTuningRanges;
	public List<Vector3> m_listTuningCamera;

	// Use this for initialization
	void Awake () {
		instance = this;
	}

	void Start()
	{
		m_hasChanged = false;
		ChangeLevel();
	}

	// Update is called once per frame
	//public override void Gamescene_Update () {

	float m_currentDeltaTime;
	public void Update(){
		//if (m_eState == FDShootyEnum.IDLE) {
			
		//}
		//UpdateFastFlippyBottle();
		UpdateHoldRelease();

		/*if (Input.GetMouseButtonDown (0)) {
			m_currentMousePos = m_currentRocketRotation - Input.mousePosition.x;// + m_rigidbodyRocket.gameObject.transform.localEulerAngles.z;
		}
		if(Input.GetMouseButton(0)){
			//Debug.Log ("Boost : " + Input.mousePosition.x);
			m_rigidbodyRocket.AddForce (m_rigidbodyRocket.transform.TransformDirection(Vector3.up * 2000 * Time.deltaTime));
			m_rigidbodyRocket.gameObject.transform.localEulerAngles = new Vector3 (0, 0, Input.mousePosition.x + m_currentMousePos );
			m_rocketBoost.SetActive (true);
		}
		if (Input.GetMouseButtonUp (0)) {
			m_rocketBoost.SetActive (false);
			m_currentRocketRotation = Input.mousePosition.x + m_currentMousePos;
		}*/
		//Debug.Log ("Speed : " + m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D> ().velocity.y);

	}

	void UpdateHoldRelease()
	{
		if (Input.GetMouseButtonUp (0) && m_eState == FDShootyEnum.CHARGE) {
			float shootStrength = Time.time - m_currentDeltaTime;
			LeanTween.cancel (m_rigidbodyRocket.gameObject);
			m_rigidbodyRocket.isKinematic = false;
			//m_rigidbodyRocket.transform.localScale = new Vector3 (100, 100, 100);
			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (100, 100, 100), 0.05f);
			m_rigidbodyRocket.AddForce (new Vector2 (30000 * shootStrength, 150000 * shootStrength));
			//m_rigidbodyRocket.AddTorque (Random.Range(5000,15000));
			//m_rigidbodyRocket.AddTorque (Random.Range(5000,105000));
			m_rigidbodyRocket.AddTorque (Random.Range(-105000,105000));

			//m_rocketBoost.SetActive (false);
			//m_currentRocketRotation = Input.mousePosition.x + m_currentMousePos;
			ZAudioMgr.Instance.PlaySFX(m_audioShoot);
			m_currentScoreIndex = 0;
			m_eState = FDShootyEnum.SHOOT;
		} else if (Input.GetMouseButtonDown (0) && m_eState == FDShootyEnum.IDLE) {
			//Debug.Log ("Boost : " + Input.mousePosition.x);
			ResetBall();
			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (100, 10, 100), 1.15f);
			LeanTween.rotateZ(this.m_rigidbodyRocket.gameObject, -15f, 0.01f);
			LeanTween.move (m_rigidbodyRocket.gameObject, m_rigidbodyRocket.gameObject.transform.localPosition + new Vector3 (-5, -10, 0), 1.15f);
			//m_rigidbodyRocket.gameObject.transform.localEulerAngles = new Vector3 (0, 0, Input.mousePosition.x + m_currentMousePos );
			//m_rocketBoost.SetActive (true);
			m_currentDeltaTime = Time.time;
			m_eState = FDShootyEnum.CHARGE;

		}
	}

	Vector2 m_initialPosition;
	Vector2 m_finalPosition;

	bool mouseDown = false;

	void UpdateFlippyBottle()
	{
		if (!mouseDown && Input.GetMouseButtonDown (0)) {
			//CoinGroupExplosion.Instance.ResetBanner ();

			mouseDown = true;
			m_initialPosition = Input.mousePosition;

			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (90, 90, 90), 0.1f);
			ZAudioMgr.Instance.PlaySFX(m_audioShoot);
			//LeanTween.rotateZ(this.m_rigidbodyRocket.gameObject, -15f, 0.01f);
			//LeanTween.move (m_rigidbodyRocket.gameObject, m_rigidbodyRocket.gameObject.transform.localPosition + new Vector3 (-5, -10, 0), 1.15f);
			//m_objectTutorial.SetActive (false);
			//m_objectEarned.SetActive (false);
			//m_objectNoInternet.SetActive (false);
			//m_objectTutorialArrow.SetActive (false);
			//m_objectTextTutorial.SetActive (false);

		} else if (mouseDown && Input.GetMouseButtonUp (0)) {

			Debug.Log ("Release");
			mouseDown = false;
			m_finalPosition = Input.mousePosition;

			ZAudioMgr.Instance.PlaySFX(m_audioShoot);

			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (100, 100, 100), 0.05f);

			float angle = Mathf.Atan2 (m_initialPosition.y - m_finalPosition.y, m_initialPosition.x - m_finalPosition.x);
			Vector3 direction = Quaternion.AngleAxis (angle * 180 / Mathf.PI, Vector3.forward) * Vector3.left * 0.5f;	

			m_rigidbodyRocket.AddTorque (Random.Range(-105000,105000));

			//Vector2 newDirection = direction * 4500f;
			Vector2 newDirection = direction * 40000f;

			Vector2 convertedInitialPosition = Camera.main.ScreenToWorldPoint (m_initialPosition);
			Vector2 convertedFinalPosition = Camera.main.ScreenToWorldPoint (m_finalPosition);
			if (Vector2.Distance (convertedInitialPosition, convertedFinalPosition) > 2) {
	
				m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
				m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 4.0f, Vector2.Distance (convertedInitialPosition, convertedFinalPosition) * 1000f));
			
				//ZAudioMgr.Instance.PlaySFX (m_audioThrow);
			}

		}
	}


	float fastFlippyTime = 0;

	void UpdateFastFlippyBottle()
	{
		if (mouseDown) {
			fastFlippyTime += Time.deltaTime;
		}

		if (!mouseDown && Input.GetMouseButtonDown (0)) {
			//CoinGroupExplosion.Instance.ResetBanner ();

			mouseDown = true;
			m_initialPosition = Input.mousePosition;

			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (90, 90, 90), 0.1f);
			//ZAudioMgr.Instance.PlaySFX(m_audioShoot);
			//LeanTween.rotateZ(this.m_rigidbodyRocket.gameObject, -15f, 0.01f);
			//LeanTween.move (m_rigidbodyRocket.gameObject, m_rigidbodyRocket.gameObject.transform.localPosition + new Vector3 (-5, -10, 0), 1.15f);
			//m_objectTutorial.SetActive (false);
			//m_objectEarned.SetActive (false);
			//m_objectNoInternet.SetActive (false);
			//m_objectTutorialArrow.SetActive (false);
			//m_objectTextTutorial.SetActive (false);
			ZAudioMgr.Instance.PlaySFX(m_audioShoot);
			fastFlippyTime = 0;

		} else if (mouseDown && (Input.GetMouseButtonUp (0) || fastFlippyTime > 0.15f)){//Input.GetMouseButtonUp (0)) {

			Debug.Log ("Release");
			mouseDown = false;
			m_finalPosition = Input.mousePosition;



			LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (100, 100, 100), 0.05f);

			float angle = Mathf.Atan2 (m_initialPosition.y - m_finalPosition.y, m_initialPosition.x - m_finalPosition.x);
			Vector3 direction = Quaternion.AngleAxis (angle * 180 / Mathf.PI, Vector3.forward) * Vector3.left * 0.5f;	

			m_rigidbodyRocket.AddTorque (Random.Range(-105000,105000));

			//Vector2 newDirection = direction * 4500f;
			Vector2 newDirection = direction * 40000f;

			Vector2 convertedInitialPosition = Camera.main.ScreenToWorldPoint (m_initialPosition);
			Vector2 convertedFinalPosition = Camera.main.ScreenToWorldPoint (m_finalPosition);
			if (Vector2.Distance (convertedInitialPosition, convertedFinalPosition) > 2) {

				m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
				m_rigidbodyRocket.gameObject.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 4.0f, Vector2.Distance (convertedInitialPosition, convertedFinalPosition) * 1000f));

				//ZAudioMgr.Instance.PlaySFX (m_audioThrow);
			}

		}
	}

	void ResetBall()
	{
		m_rigidbodyRocket.isKinematic = true;
		m_rigidbodyRocket.velocity = Vector3.zero;
		m_rigidbodyRocket.angularVelocity = 0;

		this.m_rigidbodyRocket.transform.position = m_rootRocket.transform.position;
		/*
		Vector2 tuningRange = m_listTuningRanges[0];
		Vector3 obstaclePosition;// = m_listTuning [(GameScene.instance.GetLevel ()-2) % m_listTuning.Count];
		float verticalPosition;
		//m_rootBasket.transform.localPosition = new Vector3 (tuningRange.x * (obstaclePosition.x/100f), tuningRange.y * (obstaclePosition.y/100f), m_rootBasket.transform.localPosition.z);

		//Vector3 tuneLevel = m_listTuning [(GameScene.instance.GetLevel ()-1) % m_listTuning.Count];
		if (GameScene.instance.GetLevel () <= 1) {
			this.m_rigidbodyRocket.transform.position = m_rootRocket.transform.position;
		} else {
			obstaclePosition = m_listTuning [(GameScene.instance.GetLevel ()-2) % m_listTuning.Count];
			//m_rootBasket.transform.localPosition = new Vector3 (tuningRange.x * (obstaclePosition.x/100f), tuningRange.y * (obstaclePosition.y/100f), m_rootBasket.transform.localPosition.z);
			verticalPosition = m_rootRocket.transform.position.y * (obstaclePosition.y/100f);
			this.m_rigidbodyRocket.transform.position = new Vector3 (m_rootRocket.transform.position.x, verticalPosition, m_rootRocket.transform.position.z);
		}*/
		this.m_rigidbodyRocket.transform.localEulerAngles = Vector3.zero;
		this.m_rigidbodyRocket.transform.localScale = new Vector3 (80f, 80f, 80f);
		LeanTween.scale (m_rigidbodyRocket.gameObject, new Vector3 (100f, 100f, 100f), 0.1f);
		//this.m_rigidbodyRocket.transform.localEulerAngles = new Vector3(0,0,-15f);
		//LeanTween.rotateZ(this.m_rigidbodyRocket.gameObject, -15f, 0.1f);
	}

	public override void Gamescene_ButtonDown () {
	}

	public override void Gamescene_ButtonUp () {

	}

	public override void Gamescene_Die () {
		//m_rigidbodyRocket.gravityScale = 10;
	}

	public override void Gamescene_Score () {
		if (m_eState == FDShootyEnum.SCORE)
			return;
		//ChangeLevel ();
		LeanTween.delayedCall (1.5f, ChangeLevel);
		m_eState = FDShootyEnum.SCORE;

		m_hasChanged = false;
	}

	void RandomizeBoardType()
	{
		if (Random.Range (0, 100) > 80) {
			//m_rootBasket.transform.localEulerAngles = new Vector3 (0, 270f, 0);
			m_rootBasket = m_listBaskets[1];
			m_listBaskets [1].SetActive (true);
			m_listBaskets [0].SetActive (false);
		} else {
			//m_rootBasket.transform.localEulerAngles = new Vector3 (0, 270f, 0);
			m_rootBasket = m_listBaskets[0];
			m_listBaskets [0].SetActive (true);
			m_listBaskets [1].SetActive (false);
		}
	}

	void ChangeLevel()
	{
		if (m_hasChanged)
			return;
		//m_rootBasket.transform.localPosition = new Vector3 (93.8f + Random.Range(-40,20), 38.6f + Random.Range(-80,40), 0.25f);
		//m_rootBasket.transform.localPosition = new Vector3 (93.8f + Random.Range(-50,20), 38.6f + Random.Range(-80,70), 0.25f);




		// RANGES
		// Level 1 : x = 24, y = 93
		Vector2 tuningRange = m_listTuningRanges[0];
		Vector3 obstaclePosition = m_listTuning [(GameScene.instance.GetLevel ()-1) % m_listTuning.Count];

		if (Mathf.FloorToInt (obstaclePosition.z / 10f) == 1) {
			m_rootBasket = m_listBaskets [1];
			m_listBaskets [1].SetActive (true);
			m_listBaskets [0].SetActive (false);
		} else {
			//m_rootBasket.transform.localEulerAngles = new Vector3 (0, 270f, 0);
			m_rootBasket = m_listBaskets [0];
			m_listBaskets [0].SetActive (true);
			m_listBaskets [1].SetActive (false);
		}

		m_rootBasket.transform.localPosition = new Vector3 (tuningRange.x * (obstaclePosition.x/100f), tuningRange.y * (obstaclePosition.y/100f), m_rootBasket.transform.localPosition.z);

		//RandomizeBoardType ();


		LeanTween.cancel (m_rootBasket.gameObject);
		if (obstaclePosition.z == 0) {
			LeanTween.cancel (m_rootBasket.gameObject);
		} else if (obstaclePosition.z == 1) {
			LeanTween.moveLocalX (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.x + 20, 3f).setLoopPingPong ();
		} else if (obstaclePosition.z == 2) {
			LeanTween.moveLocalY (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.y + 20, 3f).setLoopPingPong ();
		}
		else if (obstaclePosition.z == 3) {
			LeanTween.moveLocalY (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.y + 20, 3f).setLoopPingPong ();
			LeanTween.moveLocalX (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.x + 20, 3f).setLoopPingPong ();
		}
		/*if (Random.Range (0, 100) > 80) {
			LeanTween.moveLocalX (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.x + 20, 3f).setLoopPingPong ();
		}else if (Random.Range (0, 100) > 80) {
			LeanTween.moveLocalY (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.y + 20, 3f).setLoopPingPong ();
		}
		else if (Random.Range (0, 100) > 80) {
			LeanTween.moveLocalY (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.y + 20, 3f).setLoopPingPong ();
			LeanTween.moveLocalX (m_rootBasket.gameObject, m_rootBasket.transform.localPosition.x + 20, 3f).setLoopPingPong ();
		}*/

		m_hasChanged = true;

		int tuningCameraIndex = Mathf.FloorToInt ((GameScene.instance.GetLevel () - 1) / 20f);
		if (tuningCameraIndex >= m_listTuningCamera.Count-1) {
			tuningCameraIndex = m_listTuningCamera.Count-1;
		}
		Vector3 cameraSettings = m_listTuningCamera [tuningCameraIndex];
		ZCameraMgr.instance.gameObject.transform.localPosition = new Vector3 (cameraSettings.x, cameraSettings.y, -250f);
		ZCameraMgr.instance.gameObject.GetComponent<Camera> ().orthographicSize = cameraSettings.z;
	}

	bool m_hasChanged = false;
	public override void Gamescene_Setuplevel(bool hardreset) {
		/*m_rigidbodyRocket.isKinematic = true;
		m_rigidbodyRocket.velocity = Vector3.zero;
		m_rigidbodyRocket.angularVelocity = 0;
		this.m_rigidbodyRocket.transform.position = m_rootRocket.transform.position;
		this.m_rigidbodyRocket.transform.localEulerAngles = Vector3.zero;
*/

		ResetBall ();
		this.m_rigidbodyRocket.transform.localEulerAngles = Vector3.zero;
		ChangeLevel ();
		//RandomizeBoardType ();
		//ChangeLevel();



		//this.m_rigidbodyRocket.transform.position = m_rootRocket.transform.position;
		//this.m_rigidbodyRocket.transform.eulerAngles = Vector3.zero;
		//m_rocketBoost.SetActive (false);
		//m_rigidbodyRocket.gravityScale = 2;
		m_eState = FDShootyEnum.IDLE;
	}

	public override void Gamescene_Unloadlevel() {
	}
}