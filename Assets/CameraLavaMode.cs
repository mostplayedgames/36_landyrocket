﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLavaMode : MonoBehaviour 
{
	public GameObject m_floorObject;
	public bool m_isLavaRising = false;
	private int m_curLevel;
	public int INIT_LAVA_SPEED = 2;


	// Use this for initialization
	void Start () 
	{
		
	}

	public void StartLava(int p_curLevel)
	{
		m_curLevel = p_curLevel;

		if (m_curLevel % 5 == 0 &&
			m_curLevel != 0) 
		{
			INIT_LAVA_SPEED += 1;
		}

		m_isLavaRising = true;
	}

	public void StopLava()
	{
		m_isLavaRising = false;

	}

	public void Reset()
	{
		INIT_LAVA_SPEED = 3;
		m_floorObject.transform.localPosition = new Vector3 (m_floorObject.transform.localPosition.x,
															-46,
															m_floorObject.transform.localPosition.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (m_isLavaRising) 
		{
			this.transform.localPosition += new Vector3 (0, 1 * Time.deltaTime, 0);	
			m_floorObject.transform.localPosition += new Vector3 (0, INIT_LAVA_SPEED * Time.deltaTime, 0);
		}
	}
}
